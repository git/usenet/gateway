# newsgroups.al -- Logic to build Newsgroups header.  -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2005 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

newsgroups.al - Construct the Newsgroups header for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('newsgroups', 'group', 'example.test');
    $gateway->config ('newsgroups', 'map', 'example.test', '/test@/');
    my $error = $gateway->apply ('newsgroups');

=head1 DESCRIPTION

Construct the Newsgroups header for a message.  This module can work in two
modes, either attempting to deal with crossposting properly for a generic
mail to news gateway or checking an existing Newsgroups header and adding
one if the header is missing.  The latter mode is used for processing
submissions to a moderated group, and it can fall back to a generic
gatewaying mode if no Newsgroups header is present.

The simplest case is when an existing Newsgroup header is honored.  This
will be the case if there is a Newsgroups header in the incoming message and
a C<newsgroups accept> configuration directive has been given.  If at least
one of the newsgroups in the Newsgroups header matches one of the groups or
group patterns given to C<newsgroups accept>, the Newsgroups header will be
accepted as-is.  (Use the crosspost module to validate additional
crossposted groups.)  If none of the groups in the Newsgroups header matches
C<newsgroups accept> directives, the article is rejected.

If there is no Newsgroups header and there is no C<newsgroups group> header,
the article is rejected.  Otherwise, this module falls back on the algorithm
described below for determining the Newsgroups header from the To and Cc
headers.  Note that this fallback occurs only if the C<newsgroups group>
configuration directive is given.

If there is no C<newsgroups accept> header, or if the module falls through
as described above, any Newsgroups header in the message is renamed to
X-Original-Newsgroups and is ignored for the purposes of building the
final Newsgroups header.  This is to handle messages to a mail-to-news
gateway that may have also been posted to another newsgroup.  In this
case, any Message-ID header is also renamed to X-Original-Message-ID to
avoid collisions with the posted copy of the same message.

If there is no C<newsgroups accept> header, or if the module falls through
as described above, the To and Cc headers are analyzed to determine what
newsgroups the message should go to.  C<newsgroups map> directives set up
the list of associations between addresses and newsgroups, allowing correct
crossposting rather than multiposting if that map catches all addresses that
led to mail-to-news gateways.

For this to work properly, the module must be told what the primary
newsgroup is with the C<newsgroups group> directive.  This should be the
group to which we should post by default, if none of the To and Cc addresses
are recognized.  In other words, suppose you have several different mail to
news gateway addresses for several different groups which all feed into the
same program.  Configure such a system so that the program knows what
address a given message arrived via (either by having access to the envelope
recipient through using qmail, using procmail as a delivery agent, by having
each mail to news gateway alias give the program the name of its primary
newsgroup on the command line, or some other method).  Now, if none of the
To or Cc addresses correspond to any known addresses from C<newsgroups map>
directives, the module will post to that group.

Otherwise, a candidate list of groups will be built from the To and Cc
headers as described above.  If none of the resulting groups are the primary
group, the message will be posted solely to the primary group and any
Message-ID header will be renamed to X-Original-Message-ID (in the
expectation that the same message will be posted by one of the gateways
corresponding to the To and Cc addresses).  If the primary group is the
first of the groups constructed from the To and Cc headers, the message will
be posted to that full list of groups with its original Message-ID.  If the
primary group is present in the list but not the first group, the message
will be rejected with the error "not primary instance".

To see why, notice that if every gateway receiving the message crossposted
it properly, duplicate messages would be posted equal to the number of
crossposted groups.  The solution to this problem is for one of the
instances of the gateway program decide to post and all of the other ones
exit quietly.  This module supports that behavior by returning the error
"not primary instance" if the primary group is in the list formed from the
To and Cc headers but isn't the first group in that list.  The assumption is
that the gateway will also get the mail corresponding to the first group in
the list, and that instance will do the crossposting.  A program using this
module should probably exit silently if it returns "not primary instance".

Note that this module goes to some length to avoid renaming Message-ID
unless necessary; if you want to drop all incoming message IDs and generate
new ones, you should do that using the header module.

This module takes three configuration directives:

=over 4

=item newsgroups accept NEWSGROUP

=item newsgroups accept /PATTERN/

=item newsgroups accept FILE /PATTERN/

If at least one of these directives is given, this module will default to
using the existing Newsgroups header provided that at least one of the
newsgroups in that header matches an accepted group or group pattern.
PATTERN should be a valid regular expression.

If the third form of this directive is used, all newsgroups listed in the
file FILE that match the regex PATTERN will be added to the list of accepted
groups.  If there is whitespace in the lines in FILE, only the first
whitespace-delimited "word" on each line will be taken as a newsgroup name.
FILE must be an absolute path (i.e., it must begin with C</>).

=item newsgroups group NEWSGROUP

Sets the default newsgroup to post to.  As described above, if none of the
addresses in the To and Cc headers are recognized, or if this group doesn't
appear in the list of newsgroups generated by analyzing the To and Cc
headers, a Newsgroups header will be added containing only NEWSGROUP.  If
this directive is not given, articles without an existing Newsgroups header
will be rejected.  NEWSGROUP will also be used to determine whether to
reject the message with the error "not primary instance" as described above.

=item newsgroups map NEWSGROUP ( ADDRESS | /PATTERN/ )

Adds NEWSGROUP to the list of valid newsgroups and associates it with either
the e-mail address ADDRESS or the regex PATTERN.  ADDRESS is a literal
string that (case-insensitively) exactly matches the address associated with
NEWSGROUP.  PATTERN is a Perl regex that matches addresses associated with
NEWSGROUP.  These rules will be used to construct a Newsgroups header based
on the To and Cc headers.

=back

In all of the above, the /s around C<PATTERN> arguments are required, as
they allow unambiguous parsing of the configuration file directives.

There are three possible failure messages returned by this module:

=over 4

=item missing required Newsgroups header

The incoming message had no Newsgroups header and no C<newsgroups group>
configuration directive was given.

=item no accepted newsgroups in Newsgroups header

No C<newsgroups group> configuration directive was given and no groups in
the Newsgroups header matched any of the C<newsgroups accept> directives.

=item not primary instance

Although the primary group is among the groups derived from the To and Cc
headers, it isn't the first such group.  This probably means that multiple
copies of the message were received by different gateways, and this instance
should exit silently since another instance will be doing the posting.

=back

Two fatal error messages may be passed to the error() method:

=over 4

=item cannot open group file %s: %s

An error occurred while attempting to open a file from a FILE argument to a
configuration directive.

=item invalid regex /%s/: %s

An error occurred while compiling the given regex from a PATTERN argument to
a configuration directive.

=back

=cut
#'#

package News::Gateway;

##############################################################################
# Configuration directives
##############################################################################

# Helper function that takes a pattern in the form /<regex>/ and transforms it
# into an anonymous sub that takes one argument and returns true in a scalar
# context if the regex matches that argument and false otherwise.  This is a
# poor-man's qr// and should be replaced with qr// when News::Gateway requires
# Perl 5.6.
sub newsgroups_glob {
    my ($self, $regex) = @_;
    $regex = substr ($regex, 1, -1);
    my $glob;
    eval { $glob = sub { $_[0] =~ /$regex/ } };
    if ($@) {
        $self->error ("invalid regex /$regex/: $@");
    }
    return $glob;
}

# Parse the configuration directives.  The results are stored in keys in the
# $$self{newsgroups} hash.  group gets the default group, accept is a hash of
# accepted newsgroups, acceptpats is a list of subs matching groups to accept,
# and map is a list of pairs of either addresses or matching subs and
# newsgroup names.
sub newsgroups_conf {
    my ($self, $directive, @args) = @_;
    if ($directive eq 'group') {
        $$self{newsgroups}{group} = $args[0];
    } elsif ($directive eq 'accept') {
        if ($args[0] =~ m%^/.*/$%) {
            my $glob = $self->newsgroups_glob ($args[0]);
            $$self{newsgroups}{acceptpats} ||= [];
            push (@{ $$self{newsgroups}{acceptpats} }, $glob);
        } elsif ($args[0] =~ m%^/%) {
            my $glob = $self->newsgroups_glob ($args[1]);
            open (GROUPS, "< $args[0]\0")
                or $self->error ("cannot open group file $args[0]: $!");
            local $_;
            while (<GROUPS>) {
                my ($group) = split (' ', $_);
                $$self{newsgroups}{accept}{$_} = 1 if &$glob ($group);
            }
            close GROUPS;
        } else {
            $$self{newsgroups}{accept}{$args[0]} = 1;
        }
    } elsif ($directive eq 'map') {
        $$self{newsgroups}{map} ||= [];
        if ($args[1] =~ m%^/.*/$%) {
            my $glob = $self->newsgroups_glob ($args[1]);
            push (@{ $$self{newsgroups}{map} }, [ $glob, $args[0] ]);
        } else {
            push (@{ $$self{newsgroups}{map} }, [ lc ($args[1]), $args[0] ]);
        }
    }
}

##############################################################################
# Post checks
##############################################################################

# Implement the logic described in the documentation.
sub newsgroups_mesg {
    my $self = shift;
    my $article = $$self{Article};

    # If we have a list of accepted newsgroups, we may honor the existing
    # Newsgroups header.
    if ($$self{newsgroups}{accept} || $$self{newsgroups}{acceptpats}) {
        my $groups = $$self{newsgroups}{accept} || {};
        my @patterns = @{ $$self{newsgroups}{acceptpats} || [] };
        my $newsgroups = $article->header ('newsgroups');
        return 'missing required Newsgroups header'
            unless ($newsgroups || $$self{newsgroups}{group});
        my @newsgroups = split (/(?:\s*,\s*)+/, $newsgroups);
        my $group;
        my $accept = 0;
        for $group (@newsgroups) {
            if ($$groups{$group}) {
                $accept = 1;
            } else {
                my $glob;
                for $glob (@patterns) {
                    $accept = 1 if &$glob ($group);
                }
            }
            last if $accept;
        }
        return if $accept;
        return 'no accepted newsgroups in Newsgroups header'
            unless $$self{newsgroups}{group};
        $article->rename_header ('message-id', 'x-original-message-id', 'add')
            if $newsgroups;
    }

    # If there is no primary group, we can't continue.
    return 'no accepted newsgroups in Newsgroups header'
        unless $$self{newsgroups}{group};

    # We're ignoring the Newsgroups header and generating our own.
    $article->rename_header ('newsgroups', 'x-original-newsgroups', 'add');

    # What newsgroups did we get in the mail To/Cc headers?  This code is a
    # bit hard to read unless you're a LISP person.  The second map breaks
    # things up so that we're dealing with one address at a time, and the
    # first map tries to find an associated group.  This code has a *lot* of
    # problems still; it breaks down massively given the presence of commas in
    # comment fields in addresses.  We badly need real RFC 822 parsing, not
    # only for this but for lots of other things too.
    my $map = $$self{newsgroups}{map};
    @mailgroups = map {
        my $group;
        my ($address) = /<(\S+)>/;
        ($address) = split unless $address;
        for (@$map) {
            my ($match, $result) = @$_;
            if (((ref $match && &$match ($address))
                 || (!ref $match && $match eq lc ($address)))) {
                $group = $result;
                last;
            }
        }
        defined $group ? ($group) : ();
    } map {
        split /(?:\s*,\s*)+/
    } $article->header ('to'), $article->header ('cc');

    # If $$self{newsgroups}{group} isn't in @mailgroups, post only to it and
    # rename headers.  (Note that we can safely rename headers repeatedly
    # without breaking anything.)  Otherwise, post to @mailgroups iff the
    # first element is our default group.
    my %mailgroups = map { $_ => 1 } @mailgroups;
    my $main = $$self{newsgroups}{group};
    my @groups;
    if ($mailgroups{$main}) {
        return 'not primary instance' unless ($mailgroups[0] eq $main);
        @groups = @mailgroups;
    } else {
        @groups = ($main);
        $article->rename_header ('message-id', 'x-original-message-id', 'add');
    }
    $article->set_headers (newsgroups => join (',', @groups));
    return;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
