# cleanbody.al -- Various standard article body cleaning. -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2000, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

cleanbody.al - Check and clean up the body of an article for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    my $error = $gateway->apply ('cleanbody');

=head1 DESCRIPTION

This module checks the body of an incoming article and cleans it up,
attempting to remove bogus Windows character sets and unnecessary encoding.

If the original message was in quoted-printable, it will change the
Content-Transfer-Encoding header to be "8bit".  It recognizes
quoted-printable from the MIME encoding headers.  Some additional characters
from Windows Codepage 1252 are converted to ASCII equivalents.  Any Ctrl-Ms
are removed.  The module then checks that the message contains no invalid
characters outside the standard ISO 8859-X range and that no lines are
longer than 80 characters.

There are two possible failure messages:

=over 4

=item invalid characters in body (line %d)

The body contains characters outside the range [\s!-~\xa0-\xff].

=item line over 80 characters (line %d)

One or more lines in the body are over 80 characters long.

=back

This module takes no configuration directives.

=cut

package News::Gateway;

##############################################################################
# Post rewrites
##############################################################################

sub cleanbody_mesg {
    my $self = shift;
    my $quoted = lc $$self{Article}->header ('content-transfer-encoding');
    $quoted = ($quoted eq 'quoted-printable');
    local $_;

    # First pass.  We'll only need two passes if there were quoted-printable
    # continuation lines.
    my ($save, $splice, $line);
    for (@{scalar $$self{Article}->body}) {
        $line++;

        # Fix quoted-printable, which is annoying to have to deal with.
        if ($quoted) {
            if (defined $save) {
                $_ = $save . $_;
                undef $save;
            }
            s/=([0-9A-F]{2})/chr (hex $1)/eg;
            s/=\n//g;
            if (s/=$//) {
                # Continuation line.  Ugh.  Replace line with a disallowed
                # character and save this line; we'll need to splice this
                # line out later on another pass.
                $save = $_;
                $_ = "\0";
                $splice = 1;
                $line--;
                next;
            }
        }

        # Convert Microsoft smart quotes to their real counterparts.
        tr/\x91\x92\x93\x94/\`\'\"\"/;
        s/\x96/--/g;
        s/\x97/--/g;
        s/\x85/.../g;
        s/\x99/TM/g;

        # Remove CRs (DOS line endings, most likely) and delete characters.
        tr/\r\x7f//d;

        # Check for validity.  We allow any ISO 8859-1 characters.
        return "invalid characters in body (line $line)"
            if (!/^[\s!-~\xa0-\xff]*$/);
        return "line over 80 characters (line $line)" if (length ($_) > 80);
    }

    # Second pass if there were continuation lines to splice out the removed
    # lines of body text.
    if ($splice) {
        my $body = $$self{Article}->body;
        my $i;
        for ($i = 0; $i < @$body; $i++) {
            splice (@$body, $i--, 1) if ($$body[$i] eq "\0");
        }
    }

    # Fix Content-Transfer-Encoding header if we decoded quoted-printable.
    $$self{Article}->set_headers ('content-transfer-encoding' => '8bit')
        if $quoted;

    # Return success.
    return;
}

1;

=head1 BUGS

This module is very limited in its capabilities and makes a lot of
assumptions about the nature of the article.  It would need to do real MIME
parsing and understand other character sets to be generally useful.  It is
completely wrong for Unicode articles, and should probably detect Windows
1252 and tag it appropriately instead of attempting to convert it to ASCII.
The long line check should at least be configurable, as it's often
undesirable.

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2000, 2006 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
