# config.al -- Configuration directive handling.  -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2005 by Russ Allbery <eagle@eyrie.org>
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

config.al - Configuration file parsing for News::Gateway

=head1 SYNOPSIS

    my $gateway = News::Gateway->new;
    $gateway->config ('keywords', 'valid', 'meta');
    my @line = $gateway->config_parse ($line);
    $gateway->config_file ('/path/to/config/file');
    $gateway->config_line ($line);
    $gateway->config_lines (@lines);

=head1 DESCRIPTION

This module contains the code for reading and parsing configuration
directives and calling the appropriate module callback hooks.  Nearly all
News::Gateway programs will make use of this module.

A configuration directive consists of the name of the module the directive
is for, followed by some number of arguments.  For many modules, the first
argument will be a keyword specifying what part of the module to configure.
Under the hood, this module will call the _conf method of the named module
and pass the rest of the arguments to it.  So, for example, the config line:

    keywords valid meta

will call keywords_conf() and pass it 'valid' and 'meta' as arguments.  The
config() interface takes already parsed config directives; all of the other
methods take one or more lines of configuration directives, from various
sources, parse the line with config_parse(), and then pass the parsed
results on to the config() method.

B<WARNING>: Information in configuration directives is trusted by modules,
and modules may go so far as to eval things from configuration directives as
Perl code.  This means that anyone who has access to the configuration
directives read by this module has as much control over what a program does
as someone who can edit the source.  Any necessary sanity or security
checking on the content of configuration directives must be done before they
are passed off to modules, and C<config_file()> does no such checking.

=head1 FUNCTIONS

The following functions are provided by this module:

=over 4

=cut

package News::Gateway;

##############################################################################
# Methods
##############################################################################

=item config (MODULE, ARGUMENT [, ARGUMENT ...])

Calls MODULE_conf() and passes it the provided arguments.  By convention,
the first argument is normally a keyword specifying what part of the
behavior of MODULE the user wishes to configure, but this is not enforced.
config() does not do any parsing; the arguments are passed to the
configuration function of MODULE exactly as passed in.

config() must be called before any apply() it is supposed to affect.

=cut

sub config {
    my $self = shift;
    my $module = shift;
    my $method = $module . '_conf';
    $self->$method (@_);
}

=item config_parse (LINE)

Parses a single line, splitting it on whitespace, and returns the resulting
array.  Quoting with double quotes is supported for arguments that have
embedded whitespace.  Backslashes escape the next character, whatever it is,
and can be used to escape double-quotes.  Any text outside of double quotes
is automatically lowercased, but anything inside quotes is left alone.

This is the method used by config_file() and config_line() to parse
configuration files.  It probably isn't useful to call directly.

=cut
#'#

# We can't use Text::ParseWords here because it's too smart for its own good.
sub config_parse {
    my ($self, $line) = @_;
    my (@args, $snippet);
    while ($line =~ /\S/) {
        $line =~ s/^\s+//;
        $snippet = '';
        while ($line !~ /^\s/ && $line ne '') {
            my $tmp;
            if (index ($line, '"') == 0) {
                $line =~ s/^\"((?:[^\"\\]|\\.)+)\"//s
                    or $self->error ("config: parse error in '$line'");
                $tmp = $1;
            } else {
                $line =~ s/^((?:[^\\\"\s]|\\.)+)//s;
                $tmp = lc $1;
            }
            $tmp =~ s/\\(.)/$1/g;
            $snippet .= $tmp;
        }
        push (@args, $snippet);
    }
    @args;
}

=item config_line (LINE)

Parses and executes a single configuration line.  The line is passed to
config_parse() for parsing and then the results are passed to config().
For a description of how a line is parsed, see config_parse().

=cut

sub config_line {
    my ($self, $line) = @_;
    my @line = $self->config_parse ($line);
    $self->config (@line);
}

=item config_lines (LINE [, LINE ...])

Parse and execute the lines of a configuration file, given as an array.
Blank lines and lines beginning with C<#> are ignored.  All other lines are
passed to config_line() for parsing and execution.  A line is considered
continued on the next line if it ends in a backslash.  If that backlash is
inside double-quotes, the literal newline will be part of that argument.

For example, the following two lines, if passed into config_lines(), will be
parsed and executed as one configuration directive for the C<header> module,
with three arguments:

    header x-comment add \
        "This is a long comment about this newsgroup"

To read a whole file of configuration at a time, use config_file() (which
just calls this method under the hood).

=cut

sub config_lines {
    my ($self, @lines) = @_;
    while (@lines) {
        my $line = shift @lines;
        $line =~ s/^\s+//;
        next if ($line =~ /^\#/ || $line =~ /^$/);
        $line =~ s/\s+$//;
        while ($line =~ /\\$/ && @lines) {
            my $next = shift @lines;
            $next =~ s/\s+$//;
            $line =~ s/\\$/\n$next/;
        }
        $line =~ s/\\+//;
        $self->config_line ($line);
    }
}

=item config_file (FILE | HANDLE)

Reads in configuration lines from the supplied file name or file handle
(which can be either an object reference or a reference to a typeglob) until
end of file is reached, and then passes those lines to config_lines().

Multiple configuration files can be read by calling config_file() multiple
times.  One handy trick for simple scripts using News::Gateway is to put the
configuration lines at the end of the script after an __END__ and then pass
them to News::Gateway with:

    $gateway->config_file (\*DATA);

The entire file is read into memory and then parsed.  See config_parse() and
config_lines() for information on how the lines are parsed.

=cut

sub config_file {
    my ($self, $config) = @_;
    unless (ref $config) {
        open (CONFIG, $config)
            or $self->error ("config: cannot open file $config: $!");
        $config = \*CONFIG;
    }
    my @lines = <$config>;
    close CONFIG;
    $self->config_lines (@lines);
}

1;

=back

These methods adds two additional fatal error messages:

=over 4

=item config: cannot open file %s: %s

config_file() was called with a file as an argument that could not be
opened.  The operating system error message is appended.

=item config: parse error in '%s'

A configuration line could not be parsed.  The most likely cause is
unbalanced double quotes.

=back

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org> and Tim Skirvin <tskirvin@killfile.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2005 by Russ Allbery <eagle@eyrie.org>.

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>.

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
