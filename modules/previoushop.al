# previoushop.al -- Adds the previous mail hop to the Path.  -*- perl -*-
#
# Copyright 1997, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

previoushop.al - Add previous mail hop to the Path for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->apply ('previoushop');

=head1 DESCRIPTION

Adds the previous host through which a mail message passed to the Path
header of the message.  It does this by looking through the raw Received
headers, finding the first Received header that specifies a host from which
the message was received, and prepending it to the Path header (or creating
a new Path header if none already exists).

If no Received headers that contain a "from" clause are found (which may be
the case for mail originating locally), nothing is done.

The main reason for using this module is to support news to mail and mail to
news gateways for the same unmoderated newsgroup.  If the mail to news
address is subscribed to a mailing list and posts to the newsgroup are sent
back to the mailing list, a loop will be created unless something is done to
stop it.  One method of stopping such loops is to alias the hostname of the
mailing list machine out of the feed to the news to mail gateway in the news
server and then use this module to put the hostname of the mailing list
machine in the Path of all articles posted through the mail to news gateway.

There are no configuration directives or failure messages.

=cut

package News::Gateway;
use strict;

##############################################################################
# Message rewrites
##############################################################################

# Troll through the raw Received headers and find the first one that specifies
# a hostname (in the form "from hostname").  Extract hostname and add it to
# the Path header.  This is for unmoderated groups that have both news to mail
# and mail to news gateways, to avoid creating loops.
sub previoushop_mesg {
    my ($self) = @_;
    my $host;
    for (@{scalar $$self{Article}->rawheaders}) {
        if (/^Received: \s+ from \s+ ([\w.@-]+)/ixs) {
            $host = $1;
            last;
        }
    }
    if ($host) {
        my $path = $$self{Article}->header ('path');
        $path = $path ? "$host!$path" : $host;
        $$self{Article}->set_headers (path => $path);
    }
    return;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999, 2006 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
