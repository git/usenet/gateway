# mailtonews.al -- Translate e-mail into a news article.  -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

mailtonews.al - Minimal conversion from mail to news for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    my $error = $gateway->apply ('mailtonews');

=head1 DESCRIPTION

Performs a variety of rewrites, changes, and checks necessary to do the
basic conversion of a mail message into a news message.  Most of this
involves deleting or renaming headers that a news server will not accept or
would overwrite and making sure that the required news headers are present.

Only the minimal required conversion is done here.  Other cleanup that may
be frequently useful or desireable (dropping unwanted mail headers, for
example) should be done with other modules so that this module can remain as
general as possible.  Some of the modifications made here aren't strictly
required by the standards but work around bugs in common news server
implementations.

The following transformations are made:

=over 4

=item *

Checks to ensure there is a From header; unfolds the From header if it was
folded lines since INN can't deal with continuation lines in a From header.

=item *

Checks the Newsgroups header, if present, for syntactic validity, and
removes duplicate groups.

=item *

Drops the headers Date-Received, Lines, Posted, Posting-Version, Received,
Relay-Version, and Xref, since they will be rejected or overwritten by the
news server.  (This is the full list of "obsolete" headers as of INN 2.2,
along with Lines and Xref which servers will (re)generate if they wish.)

=item *

Renames NNTP-Posting-Host, NNTP-Posting-User, NNTP-Posting-Date,
Injector-Info, and Sender, if present, with an C<X-Original-> prefix to
avoid rejections due to user-supplied trace headers or confusion with the
trace headers of the moderator.

=item *

Renames Path to X-Original-Path if it contains any useful content (defined
as having at least one C<!> in the Path); otherwise, drops any existing Path
header.  Often in messages mailed to moderators, this header contains only
the unuseful C<not-for-mail> value.

=item *

Renames X-Complaints-To and X-Trace, if present, to X-Poster-Complaints-To
and X-Poster-Trace, again to avoid rejections due to user-supplied trace
headers or confusion with the trace headers of the moderator.

=item *

Renames the Message-ID header to X-Original-Message-ID if the message ID is
syntactically invalid for a news message.  This will cause a new message ID
to be generated either by the server or possibly by News::Gateway depending
on the circumstances.

=item *

Checks the In-Reply-To header if present to see if it contains something
that looks like a message ID.  If it does, adds it to the end of the
References header if and only if it doesn't duplicate the message ID on the
end of the References header.

=item *

Sets the subject of the article to the value "(none)" if there is no Subject
header.

=item *

If the first line of a folded subject header contains only whitespace,
strips all leading whitespace so that INN won't incorrectly reject the
article.

=back

Note that this module does not add a Newsgroups header if one is not already
present.  You'll need to do that elsewhere, probably with either the
newsgroups module or with a generic rule in the header module.  (While it's
tempting to assume that all submissions to a moderated group will already
have a Newsgroups header added by the news server, you'll find in practice
that people will often mail submissions directly to the submission address,
or that news servers won't handle things in the way that you would expect.)

This module also doesn't perform the standard security checks of rejecting
control messages (possibly other than cancels) or removing a pre-existing
Approved header.  These checks should be performed, if wanted, using other
modules.

There are two possible failure messages which may be returned:

=over 4

=item empty body

The body of the message was empty.  Most news servers will reject messages
with an empty body.

=item missing required From header

The incoming message doesn't have a From header.  A From header is required
for all messages by RFC 1036.

=back

This module takes no configuration directives.

=cut
#'#

package News::Gateway;

##############################################################################
# Post rewrites
##############################################################################

# Munge a mail message into a news article.  This involves making a variety
# of header changes, dropping headers that the news server won't accept in
# posts, adding a Newsgroups header if one doesn't exist, and ensuring that
# all required headers are present.  Note that we *don't* enforce RFC 1036
# compliance in the From header; this is a conscious design decision since
# some groups may not want this.  It should be done in another module.
sub mailtonews_mesg {
    my $self = shift;
    my $article = $$self{Article};

    # Make sure that we have a From header.  If not, we reject this article.
    # We also unfold the From header into spaces, since otherwise INN may
    # reject the article incorrectly.  According to RFC 822, a newline
    # followed by LWSP should be treated identically to the LWSP.
    my $from = $article->header ('from')
        or return 'missing required From header';
    if ($from =~ s/\n(\s)/$1/g) {
        $article->set_headers (from => $from);
    }

    # Make sure the body isn't empty.  (Possibly reconsider this later, as
    # news servers should accept such articles even though INN currently
    # doesn't.)
    if ($article->lines == 0) {
        return 'empty body';
    }

    # If there is a Newsgroups header, strip out whitespace and extra commas
    # and remove any duplicate groups or groups that don't match /^[\w.+-]+$/.
    my $newsgroups = $article->header ('newsgroups');
    if ($newsgroups) {
        my %seen;
        my @groups = split (/(?:\s*,\s*)+/, $newsgroups);
        @groups = grep { not $seen{$_}++ } grep { /^[\w.+-]+$/ } @groups;
        $newsgroups = join (',', @groups);
    }
    $article->set_headers (newsgroups => $newsgroups) if $newsgroups;

    # As of INN 2.2, the following headers are considered obsolete and
    # messages containing them are rejected:  Date-Received, Posted,
    # Posting-Version, Received, Relay-Version.  We drop these, as well as
    # Xref and Lines (which will be regenerated if needed).
    $article->drop_headers (qw[date-received lines posted posting-version
                               received relay-version xref]);

    # Rename trace headers.
    for (qw[nntp-posting-host nntp-posting-user nntp-posting-date
            injector-info sender]) {
        $article->rename_header ($_, "x-original-$_", 'add');
    }

    # Rename the Path header only if it contains something useful; otherwise,
    # drop it.  (Frequently it contains only not-for-mail.)
    my $path = $article->header ('path');
    if ($path) {
        if ($path =~ /!/) {
            $article->rename_header ('path', 'x-original-path', 'add');
        } else {
            $article->drop_headers ('path');
        }
    }

    # Rename the headers that sometimes begin with X-.
    for (qw[complaints-to trace]) {
        $article->rename_header ("x-$_", "x-poster-$_", 'add');
    }

    # Check the mail message ID and see if it looks reasonable.  If not,
    # rename it so that we'll generate our own.  Note that news servers may
    # reject message IDs with a trailing period, so don't allow them.  This is
    # not a complete check; a complete check should really use the code from
    # INN and needs more than a simple regex.
    my $id = $article->header ('message-id');
    $article->rename_header ('message-id', 'x-original-message-id', 'add')
        if ($id && $id !~ /^<[^\s\@>]+\@[^\s.@>]+(\.[^.\s@>]+)+>$/);

    # Many mail clients put the message ID of the message being replied to in
    # the In-Reply-To header and just carry References over.  We therefore try
    # to extract a message ID out of the In-Reply-To header and append it to
    # the References line if it isn't already there.  This is a hack, but I
    # think it's a necessary one.  This regex probably still isn't quite what
    # we want.  We don't refold the References header here.  We probably
    # should.
    $id = $article->header ('in-reply-to');
    if ($id && $id =~ /(<[^\s\@>]+\@[^\s.@>]+(\.[^.@\s>]+)+>)/) {
        $id = $1;
        my $references = $article->header ('references');
        if (!$references) {
            $article->set_headers (references => $id);
        } elsif ((split (' ', $references))[-1] ne $id) {
            $references .= ' ' . $id;
            $article->set_headers (references => $references);
        }
    }

    # Make sure we have a subject line; if the message didn't have one, we add
    # a default one of "(none)" and special-case this later on.  If the
    # Subject header is folded with no text on the first line, undo the first
    # fold so that INN won't get confused and reject the article for having no
    # Subject.
    my $subject = $article->header ('subject');
    if (!defined $subject || $subject =~ s/^\s*\n\s+//) {
        $subject = '(none)' unless defined $subject;
        $article->set_headers (subject => $subject);
    }

    # We succeeded, so return undef.
    undef;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
