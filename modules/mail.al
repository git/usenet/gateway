# mail.al -- Forward or reply to a submission.  -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

mail.al - Forward or reply to messages for News::Gateway

=head1 SYNOPSIS

    my $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->mail ('foo@example.com');
    $gateway->bounce ('invalid message');
    $gateway->filereply ('/path/to/form/message');

=head1 DESCRIPTION

This module provides functions related to sending mail.  The following
methods are provided:

=over 4

=cut

package News::Gateway;
use strict;

##############################################################################
# Methods
##############################################################################

=item mail ([ADDRESS [, ADDRESS]])

Mail the article to the provided addresses.  If no address is supplied, send
the message to the addresses given in the message (To, Cc, Bcc, and the
Resent-* variants depending on your MTA), so this method should be used
after news to mail rewrites have taken place.  This is just a wrapper around
the News::Article method; see L<News::Article> for more information.

=cut

sub mail {
    my $self = shift;
    $$self{Article}->envelope ($$self{Envelope}) if defined $$self{Envelope};
    $$self{Article}->mail (@_);
}

=item mail_bounce (ERROR)

Bounce the message, done by printing ERROR on standard error and then
exiting with the permanent failure status code (as defined by
$News::Gateway::FAILCODE).  This should only be used as a last resort,
particularly for robomoderators, since the bounces will go to the envelope
sender address which is often not the same as the poster.

=cut

sub mail_bounce {
    my ($self, $error) = @_;
    $! = $FAILCODE;
    die "$error\n";
}

=item mail_filereply (FILENAME [, SOURCES])

Mails a News::FormReply-style message to the author of the incoming message.
See L<News::FormReply> for more information.  FILENAME is the path to the
file to use to construct the reply, and SOURCES for News::FormReply
variables can optionally be provided.  The following variables are exposed
to the form message by default:

    @BODY        Message body, possibly munged by previous modules.
    @HEADERS     Current message headers.
    @OLDHEADERS  Original message headers.
    $SUBJECT     Original subject line.
    $MAINTAINER  Maintainer of this gateway.

Returns true if sending the mail succeeded, false if it failed.

=cut

sub mail_filereply {
    my $self = shift;
    my $filename = shift;
    eval { require News::FormReply };
    if ($@) { $self->error ("unable to load News::FormReply: $@") }
    my $article = $$self{Article};
    my $source = {
        BODY       => scalar ($article->body),
        HEADERS    => [ $article->headers ],
        OLDHEADERS => scalar ($article->rawheaders),
        SUBJECT    => $article->header ('subject')
    };
    my $reply = News::FormReply->new ($article, $filename, $source, @_)
        or $self->error ("unable to generate reply from $filename");
    $reply->envelope ($$self{Envelope}) if defined $$self{Envelope};
    $reply->set_headers (precedence => 'junk');
    $reply->mail;
}

1;

=back

This last method adds two additional fatal error messages:

=over 4

=item unable to generate reply from %s

News::FormReply was unable to generate a reply from the provided form and
sources.  The filename provided could be wrong, the template could have some
syntax error, or there could be some other problem.

=item unable to load News::FormReply: %s

Perl was unable to dynamically load the News::FormReply module.

=back

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2006 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
