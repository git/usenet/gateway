# clean.al -- Clean a message via News::Article::Clean.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2006 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

clean.al - Clean a message via News::Article::Clean for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('clean', 'maxrefs', 10);
    $gateway->config ('clean', 'hostname', 'killfile.org');
    $gateway->config ('clean', 'mid-prefix', 'hpo');
    $gateway->apply ('clean');

=head1 DESCRIPTION

clean.al invokes clean_article() from News::Article::Clean on the article.
It also adds a new Message-ID and Date if necessary.

This module takes three configuration directives:

=over 4

=item clean maxrefs NUMBER

Specifies the maximum number of References entries we retain before excess
entries are trimmed.  (The $MAX_REFERENCES default in News::Article::Clean.)

=item clean hostname HOSTNAME

The domain to use for generating message IDs and similar identifiers.

=item clean mid-prefix PREFIX

The prefix to use when generating message IDs.  This is used to reduce the
changes of a message ID collision between two different message ID
generators with similar algorithms.

=back

There are no failure messages.

=cut

package News::Gateway;
use strict;

use News::Article::Clean;

##############################################################################
# Configuration directives
##############################################################################

# Parse the configuration directives and set the global configuration
# parameters.
sub clean_conf {
    my ($self, $directive, $value) = @_;
    if ($directive eq 'maxrefs') {
        $News::Article::MAX_REFERENCES = $value if defined $value;
    } elsif ($directive eq 'hostname') {
        $News::Article::MY_DOMAIN      = $value if defined $value;
    } elsif ($directive eq 'mid-prefix') {
        $News::Article::MY_PREFIX      = $value if defined $value;
    }
}

##############################################################################
# Post rewrites
##############################################################################

# Call clean_article, add_message_id, and add_date.
sub clean_mesg {
    my ($self) = @_;
    my $article = $$self{Article};
    $article->clean_article;
    $article->add_message_id ($News::Article::MY_PREFIX,
                              $News::Article::MY_DOMAIN);
    $article->add_date;
    return;
}

1;

=back

=head1 REQUIREMENTS

B<News::Article::Clean>, from newslib.

=head1 BUGS

The maxrefs, hostname, and mid-prefix configuration settings are stored in
global variables that would affect not only any News::Gateway object but any
use of News::Article::Clean in the same program.  They should really be
stored as instance variables in the article object somehow.

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org> with some tweaks by Russ Allbery
<eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
