# stripheaders.al -- Strip uninteresting mail headers.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@stanford.edu>
# Copyright 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

stripheaders.al - Remove uninteresting headers for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->apply ('stripheaders');

=head1 DESCRIPTION

Removes lots of uninteresting mail headers from the article, used for
moderation programs and gateways that want to clean generally unnecessary
information from the headers.  It removes any header matching the following
patterns:

    Apparently-To
    Cc
    Content-Length
    Delivered-To
    Errors-To
    Lines
    Old-Return-Path
    Received
    Resent-*
    Return-Path
    Precedence
    Priority
    Status
    To
    Warnings-To
    X400*
    Xref
    X-400*
    X-Authentication-Warning
    X-Envelope-To
    X-Listprocessor*
    X-Loop
    X-Mailer
    X-Original-To
    X-Ph
    X-Uidl
    X-Vms-To

Since it removes To and Cc, it should be called after the newsgroups
module.  Since it removes Received, it should be called after the mailpath
module.

Suggestions of other generally uninteresting headers are welcome, but
remember that you can remove additional headers (and perform many other
transformations) using the header module.

There are no configuration directives or failure messages.

=cut

package News::Gateway;
use strict;

my $PATTERN = '^(' . join ('|', split (' ', lc (<<'EOH'))) . ')' . "\$";
    Apparently-To
    Cc
    Content-Length
    Delivered-To
    Errors-To
    Lines
    Old-Return-Path
    Received
    Resent-.*
    Return-Path
    Precedence
    Priority
    Status
    To
    Warnings-To
    X-?400.*
    Xref
    X-Authentication-Warning
    X-Envelope-To
    X-Listprocessor.*
    X-Loop
    X-Mailer
    X-Original-To
    X-Ph
    X-Uidl
    X-Vms-To
EOH

##############################################################################
# Post checks
##############################################################################

sub stripheaders_mesg {
    my ($self) = @_;
    my $article = $$self{Article};
    for ($article->header_names) {
        my $header = lc $_;
        $article->drop_headers ($header) if ($header =~ /$PATTERN/io);
    }
    return;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>, based on a list of header patterns from
Tim Skirvin <tskirvin@killfile.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
