# nohtml.al -- Reject HTML messages.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2006 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

nohtml.al - Reject HTML messages for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    my $error = $gateway->apply ('nohtml');

=head1 DESCRIPTION

This module scans a post and rejects it if it appears to be in HTML.  This
is determined by looking at the MIME headers and scanning for HTML tags
other than link tags.

There are no configuration directives and two possible failure messages:

=over 4

=item HTML message

The message says in its MIME headers that it's in HTML.

=item probable HTML message

The count of link tags plus 10x the count of <HTML> tags and parts with a
content type of text/html, divided by the number of lines, is larger than
0.5.

=back

=cut
#'#

package News::Gateway;
use strict;

##############################################################################
# Post checks
##############################################################################

sub nohtml_mesg {
    my ($self) = @_;
    my $article = $$self{Article};

    # If the main Content-Type is HTML, that's enough.
    my $ctype = $article->header ('content-type');
    if ($ctype && lc ($ctype) =~ m%text/html%) {
        return 'HTML message';
    }

    # Scan the body.  This isn't as simple as seeing how many lines are in
    # HTML.
    my ($lines, $html);
    for my $line ($article->body) {
        next if $line =~ /^\s*$/;
        $lines++;
        $html += 10 * ($line =~ m%^Content-Type:\s+text/html|<HTML>%ig);
        $html += ($line =~ m%<(?!url|a|[^>]+\@[^>]+>|[^>]+://[^>]+>)[^>]*>%ig);
    }
    if ($lines && ($html / $lines) > 0.5) {
        return 'probable HTML message';
    }
    return;
}

1;

=back

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org>, modified and tweaked by Russ Allbery
<eagle@eyrie.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
