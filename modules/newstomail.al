# newstomail.al -- Translate news articles into e-mail.  -*- perl -*-
#
# Copyright 1998, 1999, 2005 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

newstomail.al - Minimal conversion from news to mail for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('newstomail', '/path/to/mapping/file');
    my $error = $gateway->apply ('newstomail');

=head1 DESCRIPTION

Rewrites a news article into a mail message, using a set of newsgroup to
e-mail address mappings to determine which addresses to mail the resulting
message to.

This module does the following:  Checks to make sure there's a Newsgroups
header; drops Bcc and Resent-Bcc headers in the message; renames To, Cc,
Apparently-To, Resent-To, Resent-Cc, and Return-Path headers (if present)
to the same name prefixed by X-Original- since they may be misinterpreted
by e-mail software, and sets a new To header consisting of the e-mail
addresses corresponding to all the groups in the Newsgroups header that
have defined mappings.  Duplicate addresses are stripped out.

This module takes one configuration file directive:

=over 4

=item newstomail FILENAME

Specifies the file from which the list of newsgroup to address mappings
should be read, in the form newsgroup, whitespace, address.  If the
filename ends in .db, then the file will be assumed to be a Berkeley
database instead, with newsgroups as the keys and addresses as the values.

=back

There are two possible failure messages returned by this module:

=over 4

=item missing required Newsgroups header

The incoming message doesn't have a Newsgroups header.  This module only
handles posts and needs the Newsgroups header to determine what addresses
to which to send the message.

=item no newsgroup with a mapping

None of the newsgroups in the Newsgroups header had a mapping to an e-mail
address, so there's nowhere to send the message.

=back

=cut
#'#

package News::Gateway;

##############################################################################
# Configuration directives
##############################################################################

# Takes one argument, which specifies a file of newsgroup to e-mail address
# mappings.
sub newstomail_conf {
    my ($self, $mapping) = @_;
    my $split = sub { split (' ', $_[0], 2) };
    $$self{newstomail} = $self->hash_open ($mapping, $split)
        or $self->error ("Can't open mapping file $mapping: $!");
}

##############################################################################
# Post rewrites
##############################################################################

# Take the incoming news article and rewrite it into a mail message, removing
# or renaming those headers that may cause a problem in a mail system.
sub newstomail_mesg {
    my $self = shift;
    my $article = $$self{Article};

    # Make sure that we have a Newsgroups header.  If not, we reject.
    my $newsgroups = $article->header ('newsgroups')
        or return 'missing required Newsgroups header';

    # Now, pass through the Newsgroups header, adding addresses to which we're
    # going to send this post.  If we don't end up finding any mappings, we
    # reject the message.
    my @newsgroups = split (/\s*,\s*/, $newsgroups);
    my @addresses;
    for (@newsgroups) {
        my $address = $$self{newstomail}{$_};
        push (@addresses, $address) if $address;
    }
    unless (@addresses) {
        return 'no newsgroup with a mapping';
    }
    my %seen;
    @addresses = grep { !$seen{$_}++ } @addresses;

    # We need to rename any header that could possibly be taken to be a
    # recipient address, so as not to confuse our mailer.  qmail also assigns
    # special meaning to Return-Path.  We drop the Bcc lines instead of
    # renaming them, since that seems more consistent with the Bcc semantics.
    $article->drop_headers (qw(bcc resent-bcc));
    for (qw/to cc apparently-to resent-to resent-cc return-path/) {
        $article->rename_header ($_, "x-original-$_", 'add');
    }

    # Add a To header pointing to our addresses.
    $article->set_headers (to => join (', ', @addresses));

    # All done, return success.
    return undef;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
