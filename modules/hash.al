# hash.al -- Methods to read hashes from files.  -*- perl -*-
#
# Copyright 1998, 2005 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

hash.al - Load a hash from a disk file for News::Gateway

=head1 SYNOPSIS

    my $split = sub { split (' ', $_[0]) };
    my $hash = $gateway->hash_open ('/path/to/file', $split);

=head1 DESCRIPTION

B<THIS MODULE IS EXPERIMENTAL>.  The interface to this module is pretty much
guaranteed to change in later versions of News::Gateway.  Don't rely on the
current interface.  It will probably be replaced with Tie::ShadowHash.

This module provides a generic interface for loading hashes from disk files.
It can transparently handle either regular files (which are loaded into a
memory hash) or Berkeley db files (which are tied to a hash directly).  The
following method is provided:

=over 4

=cut

package News::Gateway;
use strict;

##############################################################################
# Methods
##############################################################################

=item hash_open (FILE [, SPLIT])

If C<FILE> ends in F<.db>, then it will be tied directly to a hash using
DB_File. Otherwise, it will be opened and its contents read into memory.
C<SPLIT> is an anonymous code reference that will be called with each line
and is expected to return a list consisting of the key and then the value
(or values, if there are more than one associated with a key).  If C<SPLIT>
isn't provided, each line minus the trailing newline will be taken as a key
and the value will always be 1.

fixpath() is called on FILE, so C<~> may be used.

If C<SPLIT> returns more than one value for a key, the key will be
associated with an anonymous array containing all the values.  Otherwise,
the key will be associated with the value directly.

This method returns a reference to the resulting hash, or undef if the file
could not be opened.  If the file could not be opened, the operating system
error message will be left in $!.

=cut

sub hash_open {
    my ($self, $file, $split) = @_;
    $file = $self->fixpath ($file);
    my %hash;
    if ($file =~ /\.db$/) {
        eval { require DB_File };
        if ($@) { $self->error ("hash: unable to load DB_File: $@") }
        tie (%hash, 'DB_File', $file) or return undef;
    } else {
        open (HASH, "< $file") or return undef;
        local $_;
        my ($key, @rest);
        while (<HASH>) {
            chomp;
            if (defined $split) {
                ($key, @rest) = &$split ($_);
                $hash{$key} = (@rest == 1) ? $rest[0] : [ @rest ];
            } else {
                $hash{$_}++;
            }
        }
        close HASH;
    }
    return \%hash;
}

1;

=back

This method adds one additional fatal error message:

=over 4

=item hash: unable to load DB_File: %s

hash_open() was called on a file ending in C<.db>, but the DB_File Perl
module could not be loaded.

=back

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1998, 2005 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
