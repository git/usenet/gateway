# crosspost.al -- Limit crossposts and followups.  -*- perl -*-
#
# Copyright 1997, 1999, 2000, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

crosspost.al - Limit crossposts and followups for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('crosspost', 'newsgroups', 'max', 5);
    $gateway->config ('crosspost', 'newsgroups', 'remove', 'ex.ample');
    $gateway->config ('crosspost', 'followups', 'max', 5);
    $gateway->config ('crosspost', 'followups', 'reject', 'in.valid');
    my $error = $gateway->apply ('crosspost');

=head1 DESCRIPTION

Limits crossposting and followups.  This module takes, in some ways, the
opposite approach from the newsgroups module; rather than listing newsgroups
which are allowed, it allows one to set a limit on the number of groups
crossposted, on the number of groups to which followups would go, and on
which groups can be crossposted to or to which followups can be directed.

A group can be excluded from the Newsgroups header or from the Followup-To
header in one of two ways; either messages posted to that group or messages
that direct followups to a set of newsgroups including that group can be
rejected, or that group can be silently removed from the Newsgroups and/or
Followup-To headers.  The latter behavior is generally not recommended for
robomoderators, since moderators are generally discouraged to make decisions
about where posts should be posted for the poster.  Returning the post and
asking the poster to choose a more appropriate set of groups is generally a
better approach.

This module will remove a Followup-To header (and not add one) if its
content is/would be identical to the Newsgroups header.

Since it works with a Newsgroups header, this module should be run after any
modules that construct that header.

This module takes the following configuration directives:

=over 4

=item crosspost newsgroups max COUNT

Reject any message crossposted to more than COUNT newsgroups.

=item crosspost newsgroups remove GROUP

=item crosspost newsgroups reject GROUP

Either remove GROUP silently from the Newsgroups header if present or reject
all articles crossposted to GROUP.

=item crosspost followups max COUNT

Reject any messages that would direct followups to more than COUNT
newsgroups, bearing in mind that if no Followup-To header is present,
followups would go to all groups in the Newsgroups header.  If followups are
directed to poster, that is counted as 0 groups.

=item crosspost followups remove GROUP

=item crosspost followups reject GROUP

Either remove GROUP silently from the followups if present or reject all
articles crossposted to GROUP.

=back

There are four possible failure messages returned by this module:

=over 4

=item invalid crosspost to %s

The message was crossposted to a group mentioned in a C<crosspost reject>
directive.

=item followups would go to %s

Followups to the message would go to a group mentioned in a C<followup
reject> directive.

=item excessively crossposted

The message was crossposted to more groups than allowed by a C<crosspost
max> directive.

=item followups would go to too many groups

Followups to the message would go to more groups than allowed by a
C<followup max> directive.

=back

Two fatal error messages may be passed to the error() method:

=over 4

=item unknown crosspost header %s

The second word of a crosspost configuration directive must be either
C<newsgroups> or C<followups>.

=item unknown crosspost command %s

The third word of a crosspost configuration directive must be one of
C<max>, C<remove>, or C<reject>.

=back

=cut

package News::Gateway;

##############################################################################
# Configuration directives
##############################################################################

# Limits can apply to either the newsgroups the post goes to or the newsgroups
# followups would go to (the first argument says which header they apply to).
# Limits can be in the form of one of three keywords: "max" followed by a
# number specifying a maximum number of groups, "remove" followed by a group
# name that is removed from the crosspost or followups, and "reject" followed
# by a group name that causes the message to be rejected if that group is
# crossposted to or if followups would go to it.
sub crosspost_conf {
    my ($self, $header, $command, @argument) = @_;
    $header = lc $header;
    if ($header ne 'newsgroups' && $header ne 'followups') {
        $self->error ("unknown crosspost header $header");
    }
    if ($command ne 'max' && $command ne 'remove' && $command ne 'reject') {
        $self->error ("unknown crosspost command $command");
    }
    if ($command eq 'max') {
        $$self{crosspost}{$header . $command} = $argument[0];
    } else {
        push (@{$$self{crosspost}{$header . $command}}, @argument);
    }
}

##############################################################################
# Message rewrites
##############################################################################

# Check the message to make sure that it complies with the various limits and
# group exclusions, and possibly trim out groups if so instructed.
sub crosspost_mesg {
    my $self = shift;
    my $info = $$self{crosspost};
    my @newsgroups = split (/,/, $$self{Article}->header ('newsgroups'));

    # Check the newsgroups list for anything that should be removed or which
    # would cause a rejection.
    @newsgroups = map {
        my $group = $_;
        for (@{$$info{newsgroupsreject}}) {
            return "invalid crosspost to $group" if ($_ eq $group);
        }
        (grep { $group eq $_ } @{$$info{newsgroupsremove}}) ? () : $group;
    } @newsgroups;

    # Now get our followups.  We do this afterwards, since it may just be a
    # copy of the Newsgroups header which we could have just modified.
    my $followups = $$self{Article}->header ('followup-to');
    my @followups = $followups ? split (/,/, $followups) : @newsgroups;

    # Check the followups for the same thing.
    @followups = map {
        my $group = $_;
        for (@{$$info{followupsreject}}) {
            return "followups would go to $group" if ($_ eq $group);
        }
        (grep { $group eq $_ } @{$$info{followupsremove}}) ? () : $group;
    } @followups;

    # Check the maximum crosspost and followup restrictions, if there are any.
    if ($$info{newsgroupsmax} && $$info{newsgroupsmax} < @newsgroups) {
        return 'excessively crossposted';
    } elsif ($$info{followupsmax} && $$info{followupsmax} < @followups) {
        return 'followups would go to too many groups';
    }

    # Now, set the headers to the new value.  We suppress the Followup-To
    # header if it has the same content as the Newsgroups header.
    my $newsgroups = join (',', @newsgroups);
    $followups = join (',', @followups);
    $$self{Article}->set_headers (newsgroups => $newsgroups);
    if ($followups eq $newsgroups) {
        $$self{Article}->drop_headers ('followup-to');
    } else {
        $$self{Article}->set_headers ('followup-to' => $followups);
    }

    # Return success.
    return;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999, 2000, 2006 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
