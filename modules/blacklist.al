# blacklist.al -- Blacklist messages based on From or Subject.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

blacklist.al - From and Subject blacklists for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('blacklist', 'from', '/path/to/addresses');
    $gateway->config ('blacklist', 'subject', '/path/to/keywords');
    my $error = $gateway->apply ('blacklist');

=head1 DESCRIPTION

blacklist.al implements blacklisting for News::Gateway.  It loads files full
of regular expressions to match against the From or Subject headers.  If
there is a match, a failure message will be returned.

This module takes two configuration directives:

=over 4

=item blacklist from FILENAME

Specifies a file from which to load regexes that match blacklisted From
headers.  There should be one regular expression per line.

=item blacklist subject FILENAME

Specifies a file from which to load regexes that match blacklisted From
headers.  There should be one regular expression per line.

=back

There is one possible failure message:

=over 4

=item %s is on blacklist

The contents of the header %s matched the relevant blacklist.

=back

One fatal error message may be passed to the error() method:

=over 4

=item cannot open blacklist file %s: %s

An error occurred while attempting to open the blacklist file given as the
argument to a configuration directive.

=back

=cut

package News::Gateway;
use strict;

##############################################################################
# Configuration directives
##############################################################################

# Parse the two configuration directives.  The regexes for the From blacklist
# are stored in $$self{blacklist}{from} and the regexes for the Subject
# blacklist are stored in $$self{blacklist}{subject}, both anonymous arrays.
sub blacklist_conf {
    my ($self, $type, $file) = @_;
    $file = $self->fixpath ($file);
    $$self{blacklist}{$type} ||= [];
    open (BLACKLIST, $file)
        or $self->error ("cannot open blacklist file $file: $!");
    local $_;
    while (<BLACKLIST>) {
        chomp;
        s/^\s+//;
        s/\s+$//;
        s/\#.*//;
        next unless /\S/;
        push (@{ $$self{blacklist}{$type} }, $_);
    }
    close BLACKLIST;
}

##############################################################################
# Post checks
##############################################################################

# Implement the logic described in the documentation.  We are actually capable
# of handing back multiple errors for the same article, but the News::Gatway
# interface doesn't yet support it.  Be ready when it does.
sub blacklist_mesg {
    my ($self) = @_;
    my $article = $$self{Article};
    my (%problems, @problems);
    for my $header (qw/from subject/) {
        my $value = $article->header ($header);
        next unless $value;
        for my $regex (@{ $$self{blacklist}{$header} }) {
            if ($value =~ /$regex/) {
                $problems{$header}++;
            }
        }
        push (@problems, "$header is on blacklist") if $problems{$header};
    }
    if (@problems) {
        return wantarray ? @problems : $problems[0];
    } else {
        return;
    }
}

1;

=back

=head1 NOTES

Note that (?i) at the start of a regular expression makes it
case-insensitive.

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org>, based on code by Russ Allbery, with
some reworking by Russ Allbery <eagle@eyrie.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
