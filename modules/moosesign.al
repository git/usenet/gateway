# moosesign.al -- Sign a post with PGPMoose.  -*- perl -*-
#
# Copyright 1997, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

moosesign.al - Sign a post with PGPMoose for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    open (PASS, '/path/to/pass') or die "$0: cannot open pass: $!\n";
    my $passphrase = <PASS>;
    close PASS;
    chomp $passphrase;
    $gateway->config ('moosesign', 'example.test', $passphrase);
    $gateway->apply ('moosesign');

=head1 DESCRIPTION

Does simple PGPMoose signing of messages.  This module is B<not> a full
implementation of PGPMoose; in particular, it doesn't support all of the
crosspost handling.  It looks through the groups in the Newsgroups header
and adds a PGPMoose X-Auth header for every group in the Newsgroups header
for which a PGP key has been given.

This module takes one configuration directive, which associates a PGP key
ID and passphrase with a newsgroup:

=over 4

=item moosesign NEWSGROUP PASSPHRASE KEYID

Associates the given KEYID and PASSPHRASE with NEWSGROUP.  KEYID is optional
and will default to the name of the newsgroup surrounded by spaces if not
given.  (This default will allow PGP to find the key correctly if the key ID
is something like "Moderator for group.name <address>" while avoiding
confusing group.name and group.name.other.)

=back

Note that using this module means putting the PGP passphrases for the
moderation keys in cleartext in the configuration file, in the script, or
somewhere readable by the script.

This module has no failure messages.

=cut
#'#

package News::Gateway;
use strict;

##############################################################################
# Configuration directive
##############################################################################

# Our configuration directive gives the name of the newsgroup, the passphrase
# to use for signing posts to that newsgroup, and the key ID (which is
# optional and defaults to the name of the newsgroup surrounded by spaces).
sub moosesign_conf {
    my $self = shift;
    my $newsgroup = shift;
    $$self{moosesign}{$newsgroup} = [ @_ ];
}

##############################################################################
# Post rewrites
##############################################################################

# This is a simple implementation of PGPMoose, which only signs the post for
# every group we have a key for and doesn't attempt to do anything regarding
# crossposts.
sub moosesign_mesg {
    my $self = shift;
    for (split (/,/, $$self{Article}->header ('newsgroups'))) {
        my $args = $$self{moosesign}{$_};
        $$self{Article}->sign_pgpmoose ($_, @$args) if $args;
    }
    undef;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999, 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
