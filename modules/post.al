# post.al -- Methods to post articles.  -*- perl -*-
#
# Copyright 1998, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

post.al - Post messages for News::Gateway

=head1 SYNOPSIS

    my $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->post;
    my $server = Net::NNTP->new (Reader => 0);
    $gateway->post_ihave ($server);
    $gateway->post_program ('/usr/bin/rnews');

=head1 DESCRIPTION

This module provides functions for posting messages.  The following methods
are provided:

=over 4

=cut

package News::Gateway;

##############################################################################
# Methods
##############################################################################

# Post an article via a News::Article method and a Net::NNTP connection.  This
# method is used by both post and post_ihave and is just the common code from
# those two methods.
sub post_method {
    my ($self, $method, $server) = @_;

    # If a server is provided and isn't a Net::NNTP connection, open one.
    if ($server && !ref $server) {
        eval { require Net::NNTP };
        if ($@) { $self->error ("unable to load Net::NNTP: $@") }
        my @options;
        if ($method eq 'ihave') {
            @options = (Reader => 0);
        }
        my $connection = Net::NNTP->new ($server, @options)
            or return "unable to connect to server $server";
        $server = $connection;
    }

    # Actually post, and return any resulting error messages.
    eval { $$self{Article}->$method ($server) };
    if ($@) {
        my $error = $@;
        chomp $error;
        return $error;
    } else {
        return undef;
    }
}

=item post ([ SERVER ])

Posts the article using the NNTP POST command.  Under most circumstances,
this is how you want to post an article.  SERVER, if specified, may be
either the name or IP address of a news server to use for posting or a
reference to an open Net::NNTP connection to be used.  undef is returned on
success; the error message is returned on failure.

Before this method is called, the message headers should be rewritten into
ones appropriate for a news message.

=cut

sub post {
    my ($self, $server) = @_;
    $self->post_method ('post', $server);
}

=item post_ihave ([ SERVER ])

Posts the article using the NNTP IHAVE command.  This connects to a news
server as a transfer server rather than as a client; under most
circumstances this is not what you want.  Unless you already know what IHAVE
is, you probably won't want to use this.  Date, Message-ID, and Path headers
are added to the article if not already present before posting.  The Path
header is set to C<not-for-mail>.

As above, SERVER if specified may be either the name or IP address of a news
server to use for posting or a reference to an open Net::NNTP connection.
undef is returned on success; the error message is returned on failure.

=cut

sub post_ihave {
    my ($self, $server) = @_;
    my $article = $$self{Article};
    $article->add_date unless $article->header ('date');
    $article->add_message_id unless $article->header ('message-id');
    $article->add_headers (path => 'not-for-mail')
        unless $article->header ('path');
    $self->post_method ('ihave', $server);
}

=item post_program (COMMAND [, ARGUMENT ... ])

Posts the article by running a program and passing the article to it on
stdin.  This can be used to post via B<inews> or B<rnews> if you so wish,
although using the post() method is recommended under most circumstances.
The arguments to the command should be a list of strings; this command does
not fork a shell so shell metacharacters will not be interpreted as such.

If the program exits with status 0, success is assumed and this method
returns undef.  If the program exits with non-zero status or some other
error occurs, this method returns the output of the program (both stdout and
stderr) and an error message.  The output and error message will be
newline-terminated.

If you're posting a message via B<rnews>, make sure that the message has
valid Date, Message-ID, and Path headers before calling this function.
B<rnews> requires them and this function doesn't add them.

=cut

sub post_program {
    my ($self, @program) = @_;

    # Load in the modules we require to do this and then try to run the
    # program we were told to use.
    eval { require IPC::Open3 };
    if ($@) { $self->error ("unable to load IPC::Open3: $@") }
    eval { require FileHandle };
    if ($@) { $self->error ("unable to load FileHandle: $@") }
    my $in = new FileHandle;
    my $out = new FileHandle;
    my $pid = eval { IPC::Open3::open3 ($in, $out, $out, @program) };
    if ($@) { return "cannot execute @program: $!" }

    # Okay, that was successful.  Feed the post to the program.
    local $SIG{PIPE} = 'IGNORE';
    $$self{Article}->write ($in);
    close $in;

    # Now check our error status and our output.
    my @error = <$out>;
    close $out;
    waitpid ($pid, 0);
    if ($? != 0) {
        push (@error, "@program exited with status " . ($? >> 8));
        return (wantarray ? @error : join ('', @error));
    } else {
        return undef;
    }
}

1;

=back

These methods add an additional fatal error message:

=over 4

=item unable to load %s: %s

A Perl module required for the method called could not be loaded.  The
post() and post_ihave() methods require Net::NNTP, and post_program()
requires IPC::Open3 and FileHandle.  These modules come bundled with recent
versions of Perl.

=back

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1998, 1999, 2006 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
