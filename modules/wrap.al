# wrap.al -- Word-wrap a message with long lines.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2006 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

wrap.al - Word-wrap a message with long lines for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('wrap', 80, 78);
    $gateway->apply ('wrap');

=head1 DESCRIPTION

This module will word-wrap a post to a given number of characters per line,
hopefully in a fairly pretty way (keeping quotes and wrapping on whitespace
if possible).

It takes three configuration directives:

=over 4

=item wrap limit LIMIT

Any line longer than LIMIT characters will be wrapped.  The default is 80.

=item wrap length LENGTH

Where to wrap a line at, if it's longer than LIMIT characters.  The default
is 78.

=item wrap quote QUOTE

A regular expression that matches quote characters at the beginning of a
line.  These characters will be preserved at the beginning of the wrapped
line.  The default is C<(\s{0,5}[><=|#:}])+\s*>.

=back

There are no failure messages.

=cut
#'#

package News::Gateway;
use strict;
use vars qw($WRAP_LIMIT $WRAP_LENGTH $WRAP_QUOTE);

# The defaults.
$WRAP_LIMIT  = 80;			
$WRAP_LENGTH = 76;
$WRAP_QUOTE  = '(\s{0,5}[><=|#:}])+\s*';

##############################################################################
# Configuration directives
##############################################################################

# Parse the configuration directives and set object parameters.
sub wrap_conf {
    my ($self, $directive, $value) = @_;
    if ($directive eq 'limit') {
        $$self{wrap}{limit} = $value;
    } elsif ($directive eq 'length') {
        $$self{wrap}{length} = $value;
    } elsif ($directive eq 'quote') {
        $$self{wrap}{quote} = $value;
    }
}

##############################################################################
# Post rewrites
##############################################################################

# The function that does the actual wrapping.
sub wrap_body {
    my ($self, $limit, $length, $quote, @lines) = @_;
    my (@newlines, $count);
    for my $line (@lines) {
        while (length ($line) > $limit) {
            $count++;
            my ($first, $second) = ($line =~ /^(.{$length})(.*)$/);
            if ($first =~ /^(.*)\s+(\S+)$/) {
                $first = $1;
                $second = "$2$second";
            }
            if ($first =~ /^($quote)/) {
                $second = "$1$second";
            }
            push (@newlines, $first);
            $line = $second;
        }
        push (@newlines, $line);
    }
    return (\@newlines, $count);
}

# The post rewrite hook.  Figure out the limit, line length, and quote
# characters to use and then call wrap_body to do the work.
sub wrap_mesg {
    my ($self) = @_;
    my $article = $$self{Article};
    my $limit  = $$self{wrap}{limit}  || $WRAP_LIMIT;
    my $length = $$self{wrap}{length} || $WRAP_LENGTH;
    my $quote  = $$self{wrap}{quote}  || $WRAP_QUOTE;

    # Length can't be longer than limit.
    $length = $limit if ($length > $limit);

    # Do the work.
    my @body = $article->body;
    my ($new, $count) = $self->wrap_body ($limit, $length, $quote, @body);
    $article->set_body (@{ $new });
    return;
}

1;

=back

=head1 BUGS

It would be nice if this module did word-wrapping in a more
paragraph-centric way.  Right now, it creates uneven lines and doesn't
rewrap the whole paragraph.

The algorithm may or may not do the right thing with UTF-8 text.  It will
depend on whether Perl's length primitive and C<.> regular expression
metacharacter count whole UTF-8 characters.

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org> with some rewriting and reorganization
by Russ Allbery <eagle@eyrie.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
