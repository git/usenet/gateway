# header.al -- Rewrite and trim headers.  -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2005 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

header.al - Modify article headers for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('header', 'add', 'organization', 'SNAP');
    $gateway->config ('header', 'rename', 'message-id');
    $gateway->apply ('header');

=head1 DESCRIPTION

This module implements some general header rewriting functions, including
adding, dropping, and renaming headers and replacing header contents.  It
takes several configuration directives:

=over 4

=item add HEADER CONTENT

Adds a header HEADER with content CONTENT, keeping any existing headers
with the same name.  This command can create multiple occurrances of the
same header, which isn't allowed for many news headers.

=item add-if-from FILE HEADER CONTENT

Adds a header HEADER with content CONTENT, following the same rules as
add, if and only if the message is from one of the addresses listed in the
file FILE.

=item drop HEADER

Deletes all occurences of HEADER.

=item rename HEADER

Renames the original header to X-Original-HEADER retaining the same value.

=item ifempty HEADER CONTENT

Adds a header with content CONTENT if and only if the message doesn't
already contain a header HEADER.  If HEADER is Subject, the value
C<(none)> is considered equivalent to the header being empty.

=item replace HEADER CONTENT

Replaces all existing HEADER headers with one containing CONTENT.

=item prepend HEADER CONTENT

Adds CONTENT to the beginning of the first header HEADER or creates a new
header HEADER> with content CONTENT if none already exists.

=item reject HEADER

Returns an error if HEADER is present in the incoming message.

=back

For all of add, add-if-from, ifempty, replace, and prepend, CONTENT may be
multiple words, in which case they're joined with single spaces (but you
really want to quote CONTENT as mentioned below).

For example, suppose you have a configuration file with the following
directives:

    header add      organization  "SNAP"
    header rename   message-id    
    header drop     sender        
    header replace  comment       "gateway 0.4"
    header ifempty  subject       "no subject"
    header prepend  path          "mailtonews!"

and suppose you have an incoming message with the headers:

    Path: news.example.com
    Organization: Restaurant Reviews
    Message-ID: <123142@bar.org>
    Sender: foo@bar.org
    Comment: Hello
    Comment: Hello again

After the headers module runs, the message will have a header of:

    Path: mailtonews!news.example.com
    Organization: Restaurant Reviews
    Comment: gateway 0.4
    Organization: SNAP
    Subject: no subject
    X-Original-Message-ID: <123142@bar.org>

Note in the above example that all of the header text is quoted.  That's
highly recommended for header directives in configuration files, since
otherwise all capitalization would be smashed and all whitespace reduced
to single spaces by the configuration file parser.

Note also that prepend will create a new header if none is present, so the
above prepend directive quite what you want for Path headers normally.  If
there wasn't a Path header in the original message, it would generate a
header of:

    Path: mailtonews!

Instead, add an additional "header path ifempty not-for-mail" or some
other string before the prepend directive, so that the Path header added
if none already exists will be something like:

    Path: mailtonews!not-for-mail

If you want to make sure that a certain header is always present with a
certain value, replacing any existing headers of that name, use drop and
then add (since replace will only replace existing headers with new
content but will not add a header if it does not already exists).

This module may fail and call error() with the following messages while
reading the configuration directives:

=over 4

=item cannot open address file %s: %s

The address file given to the add-if-from directive could not be opened.

=item unknown header rewrite action %s

A rewrite action was specified that isn't among those that are supported.
This probably indicates a typo.

=back

This module may fail in one way:

=over 4

=item invalid header %s

A header that was associated with a reject action in a configuration
directive was present in the incoming message.  Note that the header will
be given in all lowercase.

=back

As a side note, if you're constructing a robomoderator for a newsgroup,
dropping or renaming the Path header in incoming messages is highly
recommended.  It turns out that some news servers will add a Path header
with their hostname B<before> remailing the message to a moderator, and if
you keep that Path header when you post, the article will never propagate
back to the site of the original poster.

=cut
#'#

package News::Gateway;
use strict;

##############################################################################
# Configuration directives
##############################################################################

# Add a single header rewrite rule.  If the action is addif, the first word in
# @replacement is actually a file or database containing a list of addresses.
# Go ahead and create the hash for it.
#
# In $$self{headers}, keys beginning with a colon (guaranteed to never be in a
# header) are hashes of files for add-if-from; everything else is the name of
# a header that has a rule associated with it.
sub header_conf {
    my ($self, $action, $header, @replacement) = @_;
    my $file;
    if ($action eq 'add-if-from') {
        $file = $header;
        $header = shift @replacement;
    }
    my $actions = 'add|add-if-from|rename|ifempty|drop|replace|prepend|reject';
    unless ($action =~ /^(?:$actions)$/o) {
        $self->error ("unknown header rewrite action $action");
    }
    my $rule = [ $action, @replacement ];
    if ($action eq 'add-if-from') {
        my $hash = $self->hash_open ($file)
            or $self->error ("cannot open address file $file: $!");
        $$self{headers}{":$file"} = $hash;
        push (@$rule, $file);
    }
    push (@{$$self{headers}{lc $header}}, $rule);
}

##############################################################################
# Post rewrites
##############################################################################

# Apply all transformations to the headers requested by header lines in the
# configuration file.  We support six directives:  drop deletes a header,
# rename renames the original header to X-Original-<header>, add adds a new
# header with the given content, ifempty sets a value for a header iff the
# header is empty, replace replaces the current header contents with the given
# new content, prepend adds the new content to the beginning of the current
# header (or creates a new header if none exists), and reject returns an error
# message if that header is present.
sub header_mesg {
    my $self = shift;

    # Iterate through all rewrite rules we have saved and apply each one of
    # them individually.  Note that for each header we have an anonymous array
    # of anonymous arrays, where each anonymous array contains the action and
    # the replacement if any.  Sort so that we apply in a deterministic order
    # for testing.
    my $fixes = $$self{headers};
    my $article = $$self{Article};
    for (grep { !/^:/ } sort keys %$fixes) {
        my $fix;
        for $fix (@{$$fixes{$_}}) {
            my ($action, @content) = @$fix;
            if ($action eq 'drop') {
                $article->drop_headers ($_);
            } elsif ($action eq 'rename') {
                $article->rename_header ($_, "x-original-$_", 'add');
            } elsif ($action eq 'add') {
                $article->add_headers ($_, join (' ', @content));
            } elsif ($action eq 'add-if-from') {
                my $file = pop @content;
                my $content = join (' ', @content);
                my $hash = $$self{headers}{":$file"};
                if ($$hash{$self->from}) {
                    $article->add_headers ($_, $content);
                }
            } elsif ($action eq 'ifempty') {
                my $current = $article->header ($_);
                if ($_ eq 'subject' && $current) {
                    undef $current if $current eq '(none)';
                }
                unless ($current) {
                    my $content = join (' ', @content);
                    $article->set_headers ($_, $content);
                }
            } elsif ($action eq 'replace') {
                $article->set_headers ($_, join (' ', @content));
            } elsif ($action eq 'prepend') {
                my @current = $article->header ($_);
                my $content = join (' ', @content);
                $current[0] = $content . ($current[0] || '');
                $article->set_headers ($_, [ @current ]);
            } elsif ($action eq 'reject' && $article->header ($_)) {
                return "invalid header $_";
            }
        }
    }
    undef;
}

1;
