# xtrace.al -- Fix news trace headers for posting.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

xtrace.al - fixes X-Trace headers in News::Gateway

=head1 SYNOPSIS

    $gateway->News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('xtrace', 'X-Gateway-Trace');
    $gateway->apply ('xtrace');

=head1 DESCRIPTION

Massage X-Trace and related headers to construct a new trace header and to
rename or remove the headers so that the news server will accept the
message.  The following changes are made:

    NNTP-Posting-Host   dropped
    NNTP-Posting-User   dropped
    NNTP-Posting-Date   dropped
    X-Trace             renamed to X-Poster-Trace
    X-Modtrace          created from NNTP-Posting-* and X-Trace

The contents of X-Modtrace will be the user identity (taken from
NNTP-Posting-User, with '@' and the X-Trace host or NNTP-Posting-Host
appended if the user doesn't contain '@' and with the user portion omitted
if that header isn't present), and then the date in parens (taken from
either X-Trace or NNTP-Posting-Date).

This module takes one configuration directive:

=over 4

=item xtrace HEADER

Synthesize a header named HEADER rather than the default of X-Modtrace.  The
contents will be the same.

=back

=cut

package News::Gateway;
use strict;

##############################################################################
# Configuration directives
##############################################################################

# Takes one argument, specifying a different header name.
sub xtrace_conf {
    my ($self, $header) = @_;
    $$self{xtrace} = $header;
}

##############################################################################
# Post rewrites
##############################################################################

# Implement the rewrite as described in the documentation.
sub xtrace_mesg {
    my $self = shift;
    my $article = $$self{Article};
    my $header = $$self{xtrace} || 'X-Modtrace';

    # Gather information from the various possible headers.
    my $xtrace = $article->header ('x-trace') || '';
    my ($date, $host);
    if ($xtrace =~ /^\S+\s+\d+\s+\d+\s+(\S+)\s+\((.*)\)$/) {
        ($host, $date) = ($1, $2);
        $host = $article->header ('nntp-posting-host') || $host;
        $host = '' if $host eq '127.0.0.1';
    } elsif ($xtrace =~ /^\s*(.*),\s+(\S+)\s*$/) {
        ($date, $host) = ($1, $2);
    }
    $host ||= $article->header ('nntp-posting-host') || 'UNKNOWN';
    my $user = $article->header ('nntp-posting-user');
    if ($user) {
        my $id = ($user =~ /\@/) ? $user : "$user\@$host";
        unless ($id =~ /\@\Q$host\E/) {
            $id .= " ($host)";
        }
        $host = $id;
    }
    $date ||= $article->header ('nntp-posting-date');
    $date ||= $article->header ('date');

    # Deal with the old headers.
    $article->rename_header ('x-trace', 'x-poster-trace', 'add')
        if $article->header ('x-trace');
    $article->drop_headers ('nntp-posting-user', 'nntp-posting-host',
                            'nntp-posting-date');

    # Set our new header.
    my $value = $host;
    $value .= " ($date)" if $date;
    $article->set_headers ($header, $value);

    # We succeeded, so return undef.
    undef;
}

1;

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <eagle@eyrie.org>

Copyright 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
