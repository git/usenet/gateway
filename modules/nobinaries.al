# nobinaries.al -- Detect and reject binary files.  -*- perl -*-
#
# Copyright 1997, 1998, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

nobinaries.al - Detect and reject binary files for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    my $error = $gateway->apply ('nobinaries');

=head1 DESCRIPTION

Checks the message to see if it is or contains a binary and rejects it if it
is.  The following checks are performed:  Ensure that no Content-Type header
in the headers or the body contains the strings "application", "image",
"audio", or "video"; ensure that no Content-Transfer-Encoding header in the
headers or the body is equal to "base64", and ensure that encoded lines do
not exceed 50% of the number of lines in the body for any article with at
least 40 lines.

An encoded line is defined as a line beginning with an M (with optional
leading whitespace or quoting characters) and exactly 60 or 61 characters in
length, or a line containing no spaces, not starting with C<M> or C<~>, and
between 59 and 80 characters in length.

Lines meeting those criteria are counted separately (the first as potential
uuencoding, the second as potential base64 encoding), and if the count of
lines in any one category is over half of the total body lines, the message
is rejected.

There are no configuration directives.  The failure messages are:

=over 4

=item apparently base64-encoded

The message body is at least 40 lines and more than 50% of those lines
contain no spaces, do not begin with C<M> or C<~>, and are between 59 and 80
characters long.

=item apparently uuencoded

The message body is at least 40 lines and more than 50% of those lines start
with M (modulo whitespace and quoting) and are exactly 60 or 61 characters
long.

=item base64 encoded

The message contains a Content-Transfer-Encoding header with a value of
"base64".

=item invalid content type

The message contains a Content-Type header that contains one of the strings
"application", "image", "audio", or "video".

=back

=cut

package News::Gateway;
use strict;

############################################################################
# Post rewrites
############################################################################

# Attempt to detect and reject all binaries.  This code is derived from George
# Theall's purge-binaries script.  The following metrics are used in making
# this determination:
#
#   * Any message with a Content-Type containing the strings "application",
#     "audio", "image", or "video" is rejected, whether the header is in the
#     headers or in the body of the message.
#   * Any message with a Content-Transfer-Encoding of "base64" is rejected,
#     whether the header is in the headers or in the body.
#   * Any message with at least 50% encoded lines and at least 40 lines, where
#     encoded lines are defined as lines beginning with M and either 60 or 61
#     characters long (optionally indented or quoted) or lines containing no
#     spaces and between 59 and 80 characters long.
#
# Eventually, this really should be smarter about multipart posts....
sub nobinaries_mesg {
    my $self = shift;
    my $article = $$self{Article};

    # Check the transfer encoding.
    my $cte = $article->header ('content-transfer-encoding');
    return 'base64 encoded' if ($cte && lc $cte eq 'base64');

    # Check the content type in the main article headers.
    my $type = $article->header ('content-type');
    return 'invalid content type'
        if ($type && $type =~ /(?:application|audio|image|video)/i);

    # Now, scan the body line by line, counting possibly encoded lines, and
    # reject the message if they exceed the above parameters or if we
    # encounter a Content-Type header in the body with a bad type (or a
    # Content-Transfer-Encoding header with a bad type).
    my ($lines, $uulines, $mimelines) = (0, 0, 0);
    for (@{scalar $article->body}) {
        $lines++;
        if (/^Content-Type:\s+(application|audio|image|video)/i) {
            return 'invalid content type';
        } elsif (/^Content-Transfer-Encoding:\s+base64/i) {
            return 'base64 encoded';
        } elsif (/^(\s|>|:)*M.{60,61}\s*$/) {
            $uulines++;
        } elsif (/^[^M~]\S{59,80}\s*$/) {
            $mimelines++;
        }
    }
    if ($lines >= 40 && $uulines / $lines > 0.5) {
        return 'apparently uuencoded';
    } elsif ($lines >= 40 && $mimelines / $lines > 0.5) {
        return 'apparently base64-encoded';
    }

    # Looks like it isn't a binary!  Yay!
    return;
}

1;

=head1 BUGS

This should really do something useful with multipart/* messages, and should
understand more about MIME structure to do its job properly.  It also
currently doesn't detect yEnc posts.

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1998, 1999, 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
