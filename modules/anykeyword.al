# anykeyword.al -- Require articles to have some keyword.  -*- perl -*-
#
# Copyright 1997, 1999 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

anykeyword.al - Require subject keywords in messages for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    my $error = $gateway->apply ('anykeyword');

=head1 DESCRIPTION

This module requires that every incoming message have at least one keyword
on the subject line.  For the purposes of this check, a keyword is defined
as anything that matches:

    /^(?:Re:\s+)?\[\S+\]/

(in other words, something without spaces enclosed in square brackets at
the beginning of the subject line modulo any Re:).

There is one possible failure message:

=over 4

=item no keyword found

The Subject header of the article doesn't contain a keyword.

=back

=cut
#'#

package News::Gateway;
use strict;

##############################################################################
# Post rewrites
##############################################################################

# Make sure that the post has a keyword (any keyword is acceptable).  Keywords
# are in the form of [\S+] at the beginning of the subject line.
sub anykeyword_mesg {
    my $self = shift;
    my $subject = $$self{Article}->header ('subject');
    if ($subject && $subject =~ /^(?:Re:\s+)?\[\S+\]/) {
        return undef;
    } else {
        return 'no keyword found';
    }
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
