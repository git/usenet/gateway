# bodyheaders.al -- Extracts headers from the message body.  -*- perl -*-
#
# Copyright 1997, 1999 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

bodyheaders.al - Take headers from the article body for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('bodyheaders', 'x-no-archive');
    $gateway->apply ('bodyheaders');

=head1 DESCRIPTION

Extracts headers from the beginning of a message body and lifts them up into
the message headers.  This is to assist posters whose software makes it
difficult for them to edit the headers of their posts or mail.

This module looks for recognized headers at the beginning of the article
body.  As long as it's seen only blank lines (possibly containing
whitespace) and recognized headers, it keeps adding the headers it sees to
the main message headers.  As soon as it finds a line that isn't a
recognized header, it stops.  If it found any headers it removes everything
up to the first non-header line from the body.

This module takes one configuration directive:

=over 4

=item bodyheaders HEADER [HEADER ...]

A list of headers to look for in the beginning of the body.  The headers
are not case sensitive, and any amount of whitespace (including none) is
allowed after the colon.

=back

For example, with the directive:

    bodyheaders x-no-archive

and a message beginning with:

    x-no-archive:yes

    Comment: hello

an X-No-Archive header with content C<yes> will be added to the message
headers and the first line of the message body after this module runs will
be the line starting with C<Comment:>.  Had C<comment> also been listed in
the configuration directive, that header would have been lifted into the
article headers as well and this module would have continued looking.

Whitespace on the line before the header is not allowed, and such a line
would not be recognized as a header to be lifted to the message headers.

Normally, the header is just added to the headers of the message, adding a
duplicate header if it was already present.  If a header is required to be
unique by the Usenet article format, any header lifted from the body will
override the header already present.

=cut

package News::Gateway;

##############################################################################
# Configuration directives
##############################################################################

# Our single directive gives a list of (case-insensitive) headers that we
# should look for and elevate out of the beginning of the article body.
sub bodyheaders_conf {
    my $self = shift;
    $$self{bodyheaders} = [ map { lc $_ } @_ ];
}

##############################################################################
# Message rewrites
##############################################################################

# We examine the beginning of the body up to the first non-blank, non-header
# line looking for headers that match the ones we're suppoesd to look for.  If
# we find any, we splice the headers and any blank lines preceeding or
# following them out of the body and add the headers to the article headers
# (non-destructively).  Headers in the body override actual headers in the
# case of unique headers.
sub bodyheaders_mesg {
    my $self = shift;
    my ($lines, $found) = (0, 0);
    my $article = $$self{Article};
    my $body = $article->body;
    for (@$body) {
        $lines++;
        next if (/^\s*$/);
        if (/^(\S+):\s*(.*)$/) {
            my ($header, $value) = (lc $1, $2);
            if (grep { $_ eq $header } @{$$self{bodyheaders}}) {
                unless ($article->add_headers ($header => $value)) {
                    $article->drop_headers ($header);
                    $article->add_headers ($header => $value);
                }
                $found++;
            } else {
                last;
            }
        } else {
            last;
        }
    }
    $lines--;
    if ($found) {
        splice (@$body, 0, $lines);
    }
    return undef;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
