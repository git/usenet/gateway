# fixpath.al -- Resolve paths like the shell.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

fixpath.al - Untaint and resolve a path for News::Gateway

=head1 SYNOPSIS

    my $origpath = "~/.verimod";
    my $newpath = News::Gateway->fixpath($origpath);
    print $newpath;     # Returns "/home/tskirvin/.verimod"

    my $origpath2 = "~fish/hello.txt";
    my $newpath2 = News::Gateway->fixpath($origpath2);
    print $newpath2;    # Returns "/home/fish/hello.txt"

=head1 DESCRIPTION

This module provides a function to untaint a path and replace any C<~>
characters with the appropriate home directory in the same way that the
shell would.

=head1 FUNCTIONS

The following functions are offered in this module:

=over 4

=cut

package News::Gateway;
use strict;

##############################################################################
# Methods
##############################################################################

=item fixpath (PATH)

Untaints PATH and replaces any C<~> characters with the appropriate home
directory.  C<~> is replaced by the HOME environment variable if set, and if
not, by the home directory of the current user.  C<~I<user>> is replaced
with the home directory of I<user>.

=cut

sub fixpath {
    my ($self, $path) = @_;
    if ($path =~ s%^~([^/]*)(/|\z)%%) {
        my $user = $1;
        my $home;
        if ($user) {
            $home = (getpwnam $user)[7];
        } else {
            $home = $ENV{HOME} || (getpwuid $>)[7];
        }
        unless (defined $home) {
            $user ||= 'current user';
            $self->error ("fixpath: cannot find home directory for $user: $!");
        }
        $path = $path ? "$home/$path" : $home;
    }
    unless ($path =~ m%^([-_\@\w/.]+)$%) {
        $self->error ("fixpath: bad characters in path: $path");
    }
    return $path;
}

1;

=back

This method adds two additional fatal error messages:

=over 4

=item fixpath: cannot find home directory for %s: %s

Unable to look up the home directory for the specified user.  If that user
is C<current user>, it means that HOME was not set and the home directory of
the current UID could not be looked up.

=item fixpath: bad characters in path: %s

There was some character in the path other than alphanumerics, C<->, C<_>,
C<@>, C</>, and C<.>.  This is a very conservative set to avoid odd
problems.  Note that this check does not prevent the user from specifying
absolute paths and hence looking at arbitrary files.  This method should
only be used on paths from configuration directives, which are trusted.

=back

=head1 BUGS

It's not clear that the path check is actually very useful, and it might get
in the way of legitimate paths that people want to use.

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org>, with some reworking by Russ Allbery
<eagle@eyrie.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
