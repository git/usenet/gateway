# mailpath.al -- Generates an X-Mail-Path from Received.  -*- perl -*-
#
# Copyright 1997, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
# Based heavily on code by Andrew Gierth <andrew@erlenstar.demon.co.uk>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

mailpath.al - Generates a mail path from Received for News::Gateway

=head1 SYNOPSIS

    my $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->apply ('mailpath');

=head1 DESCRIPTION

This module generates an X-Mail-Path header from the Received headers of a
mail message.  It attempts to figure out the real name of each host that the
mail passed through based on the standard comment syntax used by most
mailers, but this problem is inherently impossible to solve completely.

X-Mail-Path is set to a !-separated list of hosts that the mail has passed
through.  If from the Received headers the name of a host appears to be
trusted, it's given; otherwise the name followed by the IP address in
brackets is given.  If the IP address isn't available, "[UNTRUSTED]" is
appended.

The envelope sender is added to the end of the X-Mail-Path if known; if not,
"UNKNOWN" is added instead.

There are no configuration directives or failure messages.

=cut

package News::Gateway;
use strict;

############################################################################
# Post rewrites
############################################################################

# Munge the received lines of the original message into an X-Mail-Path
# header and then remove them from the final article.  This is probably
# always going to be a work in progess since it parses comments.
#
# Needs to be able to cope with:
#
#   Received: from foobar (really [1.2.3.4]) by...
#   Received: from foobar (foobar.baz.com [1.2.3.4]) by...
#   Received: from foobar (actually foobar.baz.com) by...
#   Received: from foobar ([1.2.3.4]) by...
#   Received: from foobar by...
#             (peer crosschecked as: foobar.baz.com [1.2.3.4])
#   Received: from foobar by...
#             (peer crosschecked as: [1.2.3.4])
#   Received: from foobar.baz.com (1.2.3.4) by...
#   Received: from foobar.baz.com (@1.2.3.4) by...
#   Received: from foobar.baz.com (something@1.2.3.4) by...
#   Received: from foobar.baz.com (HELO foobar) (1.2.3.4) by...
#   Received: from unknown (HELO foobar) (1.2.3.4) by...
#   Received: from foobar.baz.com [1.2.3.4] by...
#   Received: from foobar by...
#
# The above headers would be transformed respectively to:
#
#   foobar[1.2.3.4]
#   foobar.baz.com
#   foobar.baz.com
#   foobar[1.2.3.4]
#   foobar.baz.com
#   foobar[1.2.3.4]
#   foobar.baz.com
#   foobar.baz.com
#   something@foobar.baz.com
#   foobar.baz.com
#   foobar[1.2.3.4]
#   foobar.baz.com[1.2.3.4]
#   foobar[UNTRUSTED]
#
# The envelope sender is then added as the rightmost element.
sub mailpath_mesg {
    my $self = shift;
    my (@path, $element);

    # Build the list of path elements from the Received headers using lots
    # of black magic regexes.
    for (@{scalar $$self{Article}->rawheaders}) {
        undef $element;
        /^Received: \s+ from \s+                # Required token
         ([\w.@-]+)? \s*                        # Identified host name (1)
         (?:
          (?:\(HELO\ (\S+)\) \s*)?              # qmail HELO argument (2)
          \((?:                                 # First comment after host
           (?:
            (?:(\S*)\@)?                        # ident information (3)
            (\d+\.\d+\.\d+\.\d+)                # qmail IP address (4)
           ) |
           (?:
            really |                            # No real host name
            ([\w.@-]+)                          # The real host name (5)
           \ )?
           \[(\d+\.\d+\.\d+\.\d+)\] |           # The IP address (6)
           actually\ ([\w.@-]+)                 # Real host name (PP) (7)
          )
          |                                     # Alternately....
          \[(\d+\.\d+\.\d+\.\d+)\]              # The IP address (8)
          |
          .* \(peer\ crosschecked\ as: \s+      # UUNet relay machines
          (?:([\w.@-]+)\s+)?                    # The real host name (9)
          \[(\d+\.\d+\.\d+\.\d+)\]              # The IP address (10)
         )?
        /ixs or next;
        $element = $9, next if $9;
        $element = $1 . "[$10]", next if $10;
        $element = $3 . '@' . $1, next if ($4 and $3 and $1 ne 'unknown');
        $element = $1, next if ($4 and $1 ne 'unknown');
        $element = $2 . "[$4]", next if ($4 and $2);
        $element = $4, next if $4;
        $element = $5, next if ($5 and index ($5, '.') > 0);
        $element = $7, next if ($7 and index ($7, '.') > 0);
        $element = $1 . "[$6]", next if $6;
        $element = $1 . "[$8]", next if $8;
        $element = $1 . '[UNTRUSTED]' if $1;
    } continue {
        push (@path, $element) if $element;
    }

    # Add the envelope sender to the end of the path if there is one,
    # otherwise add a note that we don't know.
    push (@path, $$self{Article}->envelope || 'UNKNOWN');

    # Add the X-Mail-Path header.
    $$self{Article}->add_headers ('x-mail-path' => join ('!', @path));

    # Return success.
    return;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999, 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
