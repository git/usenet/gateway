# keywords.al -- Check for required subject line keywords.  -*- perl -*-
#
# Copyright 1997, 1999, 2000, 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

keywords.al - Check the Subject header for keywords for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('keywords', 'valid', 'META');
    $gateway->config ('keywords', 'file', '/path/to/keywords');
    my $extract = sub { $_[0] =~ /^(\S+):/ };
    $gateway->config ('keywords', 'extract', $extract);
    my $error = $gateway->apply ('keywords');

=head1 DESCRIPTION

This module checks the Subject header of a message and ensures that it
starts (modulo an initial Re:) with a valid keyword.  By default, the
following keyword formats are supported:

    KEYWORD:
    KEYWORD/KEYWORD:
    [KEYWORD]
    [KEYWORD/KEYWORD]
    [KEYWORD][KEYWORD]

If multiple keywords are given, they all have to be valid.  Keywords are
checked against a list given in a file and are case-insensitive.

There are three configuration directives:

=over 4

=item keywords file FILENAME

Specifies a file from which a list of valid keywords will be read.  The
file should list all valid keywords, one per line.  (Case doesn't matter.)

=item keywords extract SUB

SUB is a reference to a sub that takes a Subject header and returns a list
of the keywords found in that header.  It replaces the default keyword
extracter described above.  Note that since this has to be a reference, it
can't be specified from inside a configuration file.

=item keywords valid KEYWORD [KEYWORD ...]

Specifies valid keywords.  Case doesn't matter.

=back

Two failure messages are possible:

=over 4

=item no keywords found

There were no keywords in the subject header of the message.

=item invalid keyword: %s

A keyword was found that wasn't in the list of valid keywords.

=back

One fatal error message may be passed to the error() method:

=over 4

=item keywords: cannot open keywords file %s: %s

An error occurred while attempting to open a file specified in a file
configuration directive.

=back

=cut

package News::Gateway;
use strict;

##############################################################################
# Configuration directives
##############################################################################

# Our single directive takes three subcommands:  "extract" specifies a
# reference to a sub that, given a subject line, returns a list of the
# keywords present in that subject line; "valid" specifies a list of valid
# keywords, and "file" specifies a file that contains the valid keywords.
sub keywords_conf {
    my $self = shift;
    my $directive = lc shift;
    if ($directive eq 'extract') {
        $$self{keywords}{code} = shift;
    } elsif ($directive eq 'valid') {
        for (@_) { $$self{keywords}{valid}{lc $_} = 1 }
    } elsif ($directive eq 'file') {
        for my $file (@_) {
            $file = $self->fixpath ($file);
            open (KEYWORDS, "< $file")
                or $self->error ("keywords: cannot open keywords file $file:"
                                 . " $!");
            local $_;
            while (<KEYWORDS>) {
                chomp;
                $$self{keywords}{valid}{lc $_} = 1;
            }
            close KEYWORDS;
        }
    }
}

##############################################################################
# Post checks
##############################################################################

# The default routine to extract keywords from a subject line.  We support the
# forms:
#
#     KEYWORD:
#     KEYWORD/KEYWORD:
#     [KEYWORD]
#     [KEYWORD/KEYWORD]
#     [KEYWORD][KEYWORD]
#
# by default.
sub keywords_parse {
    my $subject = shift;
    ($subject) = ($subject =~ /^(?:Re: )*(\S+)/);
    my @keywords;
    my ($keywords) = ($subject =~ /^\[(\S+)\]$/);
    if ($keywords) {
        @keywords = split (/\/|\]\[/, $keywords);
    } else {
        ($keywords) = ($subject =~ /^(\S+):$/);
        return () unless $keywords;
        @keywords = split ('/', $keywords);
    }
    return @keywords;
}

# Check the subject line of the article to make sure it contains one of the
# acceptable keywords.
sub keywords_mesg {
    my $self = shift;
    my $subject = $$self{Article}->header ('subject');
    my $extract = $$self{keywords}{code} || \&keywords_parse;
    my @keywords = &$extract ($subject);
    unless (@keywords) {
        return "no keywords found";
    }
    for (@keywords) {
        unless ($$self{keywords}{valid}{lc $_}) {
            return "invalid keyword: $_";
        }
    }
    return;
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999, 2000, 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
