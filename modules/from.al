# from.al -- Parse an article to find the From address.  -*- perl -*-
#
# Copyright 2005 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

from.al - Find the From address for News::Gateway

=head1 SYNOPSIS

    my $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    my $from = $gateway->from;

=head1 DESCRIPTION

This module attempts to parse the From header of a message to get the user's
e-mail address.  It is used by other News::Gateway modules that whitelist or
take other actions based on the address of the poster.

=head1 FUNCTIONS

The following functions are provided by this module:

=over 4

=cut
#'#

package News::Gateway;
use strict;

##############################################################################
# Methods
##############################################################################

=item from ()

Parses the From header of the article and returns the address, or the empty
string if parsing failed.  It tries to recognize any of:

    From: address@example.com
    From: address@example.com (Name)
    From: Name <address@example.com>

Currently, this parser is very simple-minded; it should be replaced with a
more thorough RFC 2822 parser.

=cut

sub from {
    my $self = shift;
    my $from = $$self{Article}->header ('from');
    my ($address) = ($from =~ /<(\S+)>/);
    ($address) = split (' ', $from) unless $address;
    return $address;
}

1;

=back

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
