# mungeids.al -- Munge message IDs for better threading.  -*- perl -*-
#
# Copyright 1998, 1999 by Russ Allbery <eagle@eyrie.org>
# Based on code by Christopher Davis <ckd@loiosh.kei.com>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

mungeids.al - Munge message IDs for safer gatewaying for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('mungeids', '^example\..*');
    my $error = $gateway->apply ('mungeids');

=head1 DESCRIPTION

Programmatically munges the message IDs in the Message-ID and References
headers by adding a slash-separated list of newsgroups to which the article
is going to be posted, followed by a slash, to the beginning of them.  We
also strip off any prefix that looks like a prefix we'd generate (any number
of strings that could be a newsgroup name, separated by slashes) to prevent
threads with replies from readers of multiple mungeid'd groups from
generating constantly-growing message IDs and to hopefully still get
threading right.  For the purposes of this check, we assume newsgroup names
will contain at least one period; this module may not work correctly for
newsgroup names that do not.

The purpose of this is for use with mailing list to newsgroup gateways.
Since messages to multiple mailing lists may all have the same message ID,
and since the same mailing list may be gated to Usenet in multiple places,
the message IDs of incoming mailing list messages need to be munged in
some fashion before being passed on to Usenet.  Ideally, however,
threading should be preserved, and replies to mailing list messages from
other people on the mailing list won't refer to the munged message ID.
This module therefore applies the same munge to all of the message IDs in
the References header as well, in the hope of maintaining threading and
still getting message ID uniqueness.

This is only a partial solution that works best for unidirectional gateways
from mail to news.  Responses to the news messages will reference the munged
ids and won't thread in e-mail, so other techniques should be used for
bidirectional gateways.

If the message doesn't already have a Message-ID header, this module will
generate one.

Since an accurate Newsgroups header is needed to correctly munge the IDs,
this module should run after any modules responsible for generating that.
It also needs the final Message-ID and References header, so it should run
after the mailtonews module since that module may promote message IDs from
In-Reply-To into References and discard existing Message-ID headers.

This module takes one optional configuration directive:

=over 4

=item mungeids /REGEX/ [/REGEX/ ...]

REGEX is a regex matching newsgroups which should be used to make up the
prefix that we'll be adding to message IDs.  It must begin and end with
C</>.  If this configuration directive is supplied, then only newsgroups
matching one of the supplied regexes will be used to construct the prefix.
By default, all newsgroups in the Newsgroups header will be used in the
prefix.

REGEX is 

=back

There are no failure messages.  One fatal error message may be passed to the
error() method:

=over 4

=item invalid regex /%s/: %s

An error occurred while compiling the given regex from a REGEX argument to a
configuration directive.

=back

=cut

package News::Gateway;
use strict;

##############################################################################
# Configuration directives
##############################################################################

# We take a single optional directive that gives a list of regexes of groups
# to used in the message ID munge.  Regexes are expected to begin and end with
# / (to be consistent with how we specify regexes elsewhere), and they can
# have the standard regex flags.  This means that *we trust supplied regexes
# to be used as Perl code*!
sub mungeids_conf {
    my ($self, @masks) = @_;
    for (@masks) {
        my $regex = $_;
        $regex = substr ($regex, 1, -1);
        my $glob;
        eval { $glob = sub { $_[0] =~ /$regex/ } };
        if ($@) {
            $self->error ("invalid regex $_: $@");
        }
        push (@{$$self{mungeids}}, $glob);
    }
}

##############################################################################
# Post rewrites
##############################################################################

# Munge the Message-ID header and all message IDs in the References header to
# begin with the newsgroup name to which they were posted and a /.  This is so
# that the same message sent to multiple lists won't cause collisions, so that
# two gateways of the same list to different groups won't collide, and so that
# despite the fact that we're changing the incoming message IDs from what
# other people on the list will see, messages should hopefully still thread.
#
# Since the mailtonews module does magic with In-Reply-To to migrate things
# into References, it should run first; we don't deal with In-Reply-To here.
# We also want to already have the Newsgroups header.
sub mungeids_mesg {
    my $self = shift;

    # Figure out what prefix to use, filtering the groups through the patterns
    # set in our configuration directive if any.
    my @prefix = split (/,/, $$self{Article}->header ('newsgroups'));
    if ($$self{mungeids}) {
        @prefix = grep {
            my $group = $_;
            (grep { &$_ ($group) } @{$$self{mungeids}}) ? $group : ()
        } @prefix;
    }
    return undef unless @prefix;
    my $prefix = join ('/', (sort @prefix), '');

    # Fix up the message ID, generating one if we don't have one.
    my $messageid = $$self{Article}->header ('message-id');
    if ($messageid) {
        $messageid =~ s%^<(?:[a-z0-9_+-]+\.[a-z0-9_+.-]+/)*%<$prefix%;
        $$self{Article}->set_headers ('message-id' => $messageid);
    } else {
        $$self{Article}->add_message_id ($prefix);
    }

    # Munge all message IDs in the References header which weren't already
    # munged, if there is such a header.  Note that we have to then fold the
    # References header since there is often a line length limit on headers in
    # the news server.
    #
    # When adding our prefix, we also strip away anything that looks like a
    # prefix (anything that looks like a newsgroup name followed by a slash)
    # to prevent a thread posted separately to several groups both using ID
    # munging from acquiring more prefixes each followup.
    my $references = $$self{Article}->header ('references');
    if ($references) {
        my @references = split (' ', $references);
        my $length = 4;
        $references = '';
        for (@references) {
            s%^<(?:[a-z0-9_+-]+\.[a-z0-9_+.-]+/)*%<$prefix%;
            $length += 1 + length $_;
            $references .= ($length < 72 ? ' ' : "\n\t") . $_;
            $length = length $_ if ($length >= 72);
        }
        $references =~ s/^\s+//;
        $$self{Article}->set_headers (references => $references);
    }
    return;
}

1;

=head1 BUGS

This is a cute way of solving the problem and works okay in some situations,
but one really has to rewrite all message IDs and do it both on the mail to
news end and the news to mail end.  The best way is to maintain a database
of previously seen message IDs and their mappings rather than trying to do a
reversible transformation.

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1998, 1999, 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
