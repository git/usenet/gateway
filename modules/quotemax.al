# quotemax.al -- Allow only a certain level of quoting.  -*- perl -*-
#
# Copyright 2005 Tim Skirvin <tskirvin@killfile.org>
# Copyright 2006 Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

quotemax.al - Allow only a certain level of quoting in News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->new;
    $gateway->read (\*STDIN);
    $gateway->config ('quotemax', 'limit', 75);
    $gateway->config ('quotemax', 'minlines', 20);
    $gateway->config ('quotemax', 'regex', '(\s{0,5}[><=|#:}])+\s*');
    my $error = $gateway->apply ('quotemax');

=head1 DESCRIPTION

quotemax.al scans a post and determines what percentage of the article is
quoted from previous messages.  If this exceeds a certain percentage, then
the post is rejected.  This check is skipped if the article body is shorter
than a certain number of lines.  Blank lines are not counted as body lines;
the quoted percentage is the percentage of non-blank lines.

This module takes three configuration directives:

=over 4

=item quotemax limit LIMIT

Sets the allowable percentage of quoted material to LIMIT.  The default
limit is 95%, which allows almost everything except messages that are
entirely quoted material.

=item quotemax minlines MINIMUM

Messages with a body of fewer than MINIMUM non-blank lines are not checked
and always pass this check.  The default value is 0, meaning that all
messages are checked.

=item quotemax regex REGEX

The regular expression that matches quoted lines.  You may need to tweak
this if non-standard quote characters are common in your application.  The
default regex is C<< (\s{0,5}[><=|#:}])+\s* >>.

=back

There is one possible failure message:

=over 4

=item excessive quoting (%d/%d, %s > %s)

The quoting in the article exceeds the configured limits.  The number of
quoted body lines and the total body lines are shown, as is the percentage
quoted and the allowable percentage.

=back

=cut

package News::Gateway;
use strict;
use vars qw($QUOTECHARS $QUOTEMAX $QUOTELINES);

# The defaults.
$QUOTEMAX   ||= 95;
$QUOTELINES ||= 0;
$QUOTECHARS ||= '(\s{0,5}[><=|#:}])+\s*';

##############################################################################
# Configuration directives
##############################################################################

# Parse the confsiguration directives and set object parameters.
sub quotemax_conf {
    my ($self, $directive, $value) = @_;
    if ($directive eq 'limit') {
        $$self{quotemax}{limit} = $value;
    } elsif ($directive eq 'minlines') {
        $$self{quotemax}{minlines} = $value;
    } elsif ($directive eq 'regex') {
        $$self{quotemax}{regex} = $value;
    }
}

##############################################################################
# Post checks
##############################################################################

# Check the message as described in the documentation.
sub quotemax_mesg {
    my ($self) = @_;
    my $article = $$self{Article};
    my $limit = defined ($$self{quotemax}{limit})
        ? $$self{quotemax}{limit}
        : $QUOTEMAX;
    my $minlines = defined ($$self{quotemax}{minlines})
        ? $$self{quotemax}{minlines}
        : $QUOTELINES;
    my $regex = defined ($$self{quotemax}{regex})
        ? $$self{quotemax}{regex}
        : $QUOTECHARS;

    my ($lines, $quotes);
    for my $line ($article->body) {
        $quotes++ if     $line =~ m%^$regex%;
        $lines++  unless $line =~ m%^\s*$%;
    }
    if ($lines >= $minlines && $quotes > ($limit / 100 * $lines)) {
        my $error = sprintf ('excessive quoting (%d/%d, %.2f%% > %.2f%%)',
                             $quotes, $lines, ($quotes / $lines * 100),
                             $limit);
        return $error;
    } else {
        return;
    }
}

1;

=head1 AUTHOR

Tim Skirvin <tskirvin@killfile.org> with some reworking by Russ Allbery
<eagle@eyrie.org>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Tim Skirvin <tskirvin@killfile.org>

Copyright 2006 Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
