# whitelist.al -- Check the From line against valid posters.  -*- perl -*-
#
# Copyright 1997, 1999, 2006 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.  This is a News::Gateway module and
# requires News::Gateway to be used.

=head1 NAME

whitelist.al - Only allow posters from a whitelist for News::Gateway

=head1 SYNOPSIS

    $gateway = News::Gateway->read;
    $gateway->read (\*STDIN);
    $gateway->config ('whitelist', '/path/to/addresses');
    my $error = $gateway->apply ('whitelist');

=head1 DESCRIPTION

Checks to make sure that the incoming message is from a poster in a file
of valid posters.  The most common use of this module is in conjunction
with human moderation; all messages from pre-approved posters are sent
straight through via the robomoderator and the rest are relayed to a human
moderator for hand checking.

This module takes one configuration directive:

=over 4

=item whitelist FILE

FILE is a list of poster addresses, one per line.  Only the address should
be included in this file, not the name or other comments.  Files ending in
C<.db> are reserved for future implementations (eventually, these will
automatically be recognized as Berkeley db files for handling large lists of
posters).  Blank lines and lines beginning with C<#> are ignored.

=back

There is one possible failure message:

=over 4

=item unknown poster %s

The message is from a poster adddress that isn't in the whitelist of allowed
addresses.

=back

One fatal error message may be passed to the error() method:

=over 4

=item cannot open whitelist file %s: %s

An error occurred while trying to open the whitelist file given as the
argument to a configuration directive.

=back

=cut
#'#

package News::Gateway;
use strict;

##############################################################################
# Configuration directives
##############################################################################

# Our single directive takes a file that contains a list of acceptable
# posters.
sub whitelist_conf {
    my ($self, $whitelist) = @_;
    open (WHITELIST, $whitelist)
        or $self->error ("cannot open whitelist file $whitelist: $!");
    local $_;
    while (<WHITELIST>) {
	chomp;
        s/^\s+//;
        s/\s+$//;
        s/\#.*//;
        next unless /\S/;
	$$self{whitelist}{lc $_} = 1;
    }
    close WHITELIST;
}


##############################################################################
# Post checks
##############################################################################

# Check the address in the From line of the article against the list of valid
# posters.
sub whitelist_mesg {
    my ($self) = @_;
    my $address = $self->from;
    if ($$self{whitelist}{lc $address}) {
        return;
    } else {
        return "unknown poster $address";
    }
}

1;

=head1 AUTHOR

Russ Allbery <eagle@eyrie.org>

=head1 COPYRIGHT AND LICENSE

Copyright 1997, 1999, 2006 by Russ Allbery <eagle@eyrie.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

=cut
