# t/driver.pl -- Test driver library for News::Gateway.
#
# Copyright 1999, 2005 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.
#
# This file provides a set of functions for use in News::Gateway test scripts.
# It isn't a standalone script.
#
# Most tests will be structured as a test file of the following format: The
# first line is either "pass" by itself or "fail" followed by a space, the
# failing module, a colon, a space, and the error message it should produce.
# The second line should contain a list of all modules to use.  Subsequent
# lines up to the divider should be configuration directives.  Then a line of
# ten equals and the article, followed by a line of ten equals.  If the first
# line is pass, there should then be another article that should be identical
# to the result of the rewrites, ending with a final line of ten equals.

# Run a single test, taking as arguments the test number and the name of the
# test file and printing out the appropriate ok or not ok.
sub test_rewrite {
    my ($test, $data) = @_;
    my ($pass, $error);
    do { $pass = <$data> } while (defined ($pass) && $pass =~ /^(?:\s*$|\#)/);
    chomp $pass;
    if ($pass =~ s/^fail //) {
        $error = $pass;
        $pass = 0;
    }
    my @modules = split (' ', scalar <$data>);

    # Create a News::Gateway object and start processing configuration
    # directives.
    my $gateway = News::Gateway->new;
    local $_;
    my @config;
    while (<$data>) {
        last if $_ eq ('=' x 10 . "\n");
        push (@config, $_);
    }
    $gateway->config_lines (@config);

    # Read in the article and the expected result.
    my (@article, @expected);
    while (<$data>) {
        last if $_ eq ('=' x 10 . "\n");
        push (@article, $_);
    }
    if ($pass) {
        while (<$data>) {
            last if $_ eq ('=' x 10 . "\n");
            push (@expected, $_);
        }
    }

    # Run it through News::Gateway and check to make sure we got the expected
    # result.
    $fail = 'reading', goto FAIL unless $gateway->read (\@article);
    $status = $gateway->apply (@modules);
    if ($pass) {
        $fail = "error $status", goto FAIL if $status;
        my $article = $gateway->article;
        my @headers = map { my @l = split "\n"; @l } $article->headers;
        my @result = map { "$_\n" } @headers, '', $article->body;
        my $i;
        for ($i = 0; $i < @result; $i++) {
            if ($result[$i] ne $expected[$i]) {
                chomp $result[$i];
                chomp $expected[$i];
                $fail = 'line ' . ($i + 1) . " differs\n"
                    . "  #      saw: '$result[$i]'\n"
                    . "  # expected: '$expected[$i]'";
                goto FAIL;
            }
        }
        $fail = 'output is too short', goto FAIL if $i != @expected;
    } else {
        $fail = "wrong error $status", goto FAIL if $status ne $error;
    }
    print "ok $test\n";
    return 1;

  FAIL:
    print "not ok $test\n";
    print "  # $fail\n";
    return;
}

# Start by loading the News::Gateway module and then run a set of rewrite
# tests, given a set of test posts formatted as described above.  Takes the
# data stream to use.  Each test case is separated by a line of 60 #s, and
# before them all is a line with the total number of tests.
sub test_rewrites {
    my $data = shift;
    my $total = <$data>;
    $total = <$data> while (defined ($total) && $total =~ /^\s*$/);
    chomp $total;

    # Ensure the module can load and print out our total test count.
    print "1..", $total + 1, "\n";
    eval { require News::Gateway };
    print 'not ' if $@;
    print "ok 1\n";

    # Run our tests.
    my $test = 1;
    while ($total) {
        my $line = <$data>;
        $line = <$data> while (defined ($line) && $line =~ /^\s*$/);
        die "Invalid test format" unless (index ($line, '#' x 60) == 0);
        test_rewrite (++$test, $data);
        $total--;
    }
}

1;
