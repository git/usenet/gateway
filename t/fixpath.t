#!/usr/bin/perl
# fixpath.t -- Test for the fixpath utility module.

$| = 1;
use lib qw(blib/lib ../blib/lib);
use News::Gateway;

sub ok_string {
    my ($n, $wanted, $seen) = @_;
    if ($wanted ne $seen) {
        print "not ok $n\n  wanted: $wanted\n    seen: $seen\n";
    } else {
        print "ok $n\n";
    }
}

print "1..5\n";

my $home = $ENV{HOME} || (getpwuid $>)[7];
my $user = getpwuid $>;
my $userhome = (getpwnam $user)[7];

my $gateway = News::Gateway->new;
ok_string (1, $home, $gateway->fixpath ('~'));
ok_string (2, $userhome, $gateway->fixpath ("~$user"));
ok_string (3, "$home/foo/blah", $gateway->fixpath ('~/foo/blah'));

eval { $gateway->fixpath ('~/~') };
ok_string (4, "fixpath: bad characters in path: $home/~\n", $@);

my $bogus = "i've,really-got|to+be:kidding";
eval { $gateway->fixpath ("~$bogus") };
my $error = $@;
$error =~ s/: [^:]+$//;
ok_string (5, "fixpath: cannot find home directory for $bogus", $error);
