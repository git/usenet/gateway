#!/usr/bin/perl
# crosspost.t -- Test for crosspost rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

8

##########################################################################
# Basic functionality                                                  (2)

pass
crosspost
crosspost newsgroups remove invalid.one
crosspost newsgroups remove invalid.two
crosspost newsgroups max 3
crosspost followups remove invalid.three
crosspost followups max 2
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.one,invalid.two,invalid.three,example,junk

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.three,example,junk
Followup-To: example,junk

This is some message content.
==========


##########################################################################
# Remove redundant Followup-To                                         (3)

pass
crosspost
crosspost newsgroups max 1
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.test
Followup-To: invalid.test

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.test

This is some message content.
==========


##########################################################################
# Reject a group in Newsgroups                                         (4)

fail crosspost: invalid crosspost to invalid.test
crosspost
crosspost newsgroups reject invalid.test
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.test

This is some message content.
==========


##########################################################################
# Reject a group in Followup-To                                        (5)

fail crosspost: followups would go to invalid.test
crosspost
crosspost followups reject invalid.test
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.announce
Followup-To: invalid.test

This is some message content.
==========


##########################################################################
# Reject a group moved into Followup-To                                (6)

fail crosspost: followups would go to invalid.test
crosspost
crosspost followups reject invalid.test
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.test

This is some message content.
==========


##########################################################################
# Excessive crossposting                                               (7)

fail crosspost: excessively crossposted
crosspost
crosspost newsgroups max 2
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.test,example,junk

This is some message content.
==========


##########################################################################
# Excessive followups                                                  (8)

fail crosspost: followups would go to too many groups
crosspost
crosspost followups max 2
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.announce
Followup-To: invalid.test,example,junk

This is some message content.
==========


##########################################################################
# Excessive followups (from Newsgroups)                                (9)

fail crosspost: followups would go to too many groups
crosspost
crosspost followups max 2
==========
From: Russ Allbery <eagle@eyrie.org>
Newsgroups: invalid.test,example,junk

This is some message content.
==========
