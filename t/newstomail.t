#!/usr/bin/perl
# newstomail.t -- Test for newstomail rewrite module.  -*- perl -*-

# Load our driver module and run our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
chdir 't' if (!-d 'data' && -d 't/data');
require "driver.pl";
test_rewrites (\*DATA);

__END__

3

##########################################################################
# Basic functionality                                                  (2)

pass
newstomail
newstomail data/newstomail
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test mailtonews message
Bcc: example@example.org
Resent-Bcc: example@example.org
To: gateway@example.com
Newsgroups: example.test,example.foo,example.bar
Cc: foo@example.com
Resent-To: eagle@eyrie.org
Resent-Cc: invalid@example.org
Apparently-To: invalid@example.com
Return-Path: nobody@stanford.edu

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test mailtonews message
To: test@example.org, foo@example.org
Newsgroups: example.test,example.foo,example.bar
X-Original-Apparently-To: invalid@example.com
X-Original-Cc: foo@example.com
X-Original-Resent-Cc: invalid@example.org
X-Original-Resent-To: eagle@eyrie.org
X-Original-Return-Path: nobody@stanford.edu
X-Original-To: gateway@example.com

This is some message content.
==========


##########################################################################
# Newsgroups header required                                           (3)

fail newstomail: missing required Newsgroups header
newstomail
==========
Subject: Test mailtonews message
To: gateway@example.com
From: example@example.com

This is some message content.
==========


##########################################################################
# No newsgroups with a mapping                                         (4)

fail newstomail: no newsgroup with a mapping
newstomail
newstomail data/newstomail
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test mailtonews message
To: gateway@example.com
Newsgroups: example.baz

This is some message content.
==========
