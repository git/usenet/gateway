#!/usr/bin/perl
# newsgroups.t -- Test for newsgroups rewrite module.  -*- perl -*-

# Load our driver module and run our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

10

##########################################################################
# Default group                                                        (2)

pass
newsgroups
newsgroups group example.test
==========
From: Russ Allbery <eagle@eyrie.org>
To: example@example.org
Subject: testing
Message-ID: <test@example.org>

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: example@example.org
Subject: testing
Newsgroups: example.test
X-Original-Message-ID: <test@example.org>

This is some message content.
==========


##########################################################################
# Default group with renaming                                          (3)

pass
newsgroups
newsgroups group example.test
==========
From: Russ Allbery <eagle@eyrie.org>
To: example@example.org
Subject: testing
Newsgroups: example.local
Message-ID: <test@example.org>

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: example@example.org
Subject: testing
Newsgroups: example.test
X-Original-Message-ID: <test@example.org>
X-Original-Newsgroups: example.local

This is some message content.
==========


##########################################################################
# Named accept groups                                                  (4)

pass
newsgroups
newsgroups accept example.test
newsgroups map example.local example@example.org
==========
From: Russ Allbery <eagle@eyrie.org>
To: example@example.org
Subject: testing
Message-ID: <test@example.org>
Newsgroups: example.test

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: example@example.org
Subject: testing
Message-ID: <test@example.org>
Newsgroups: example.test

This is some message content.
==========


##########################################################################
# Regex accept groups                                                  (5)

pass
newsgroups
newsgroups accept /^example\./
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: testing
Message-ID: <test@example.org>
Newsgroups: example.test,local.foo

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: testing
Message-ID: <test@example.org>
Newsgroups: example.test,local.foo

This is some message content.
==========


##########################################################################
# Regular mail to news scenario                                        (6)

pass
newsgroups
newsgroups group example.test
newsgroups map example.test test@example.org
newsgroups map example.config config@example.com
newsgroups map example.local /.*@local\.example\.com$/
==========
Message-ID: <test@example.org>
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>

This is some message content.
==========
Message-ID: <test@example.org>
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>
Newsgroups: example.test,example.config,example.local

This is some message content.
==========


##########################################################################
# No matching addresses                                                (7)

pass
newsgroups
newsgroups group example.test
newsgroups map example.test test@example.org
newsgroups map example.config config@example.com
newsgroups map example.local /.*@local\.example\.com$/
==========
Message-ID: <test@example.org>
To: Foo <test@example.com>, config@example.org (Bar)
Cc: <baz@local.example.org>

This is some message content.
==========
To: Foo <test@example.com>, config@example.org (Bar)
Cc: <baz@local.example.org>
Newsgroups: example.test
X-Original-Message-ID: <test@example.org>

This is some message content.
==========


##########################################################################
# Group not among generated group list                                 (8)

pass
newsgroups
newsgroups group example.foo
newsgroups map example.test test@example.org
newsgroups map example.config config@example.com
newsgroups map example.local /.*@local\.example\.com$/
==========
Message-ID: <test@example.org>
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>

This is some message content.
==========
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>
Newsgroups: example.foo
X-Original-Message-ID: <test@example.org>

This is some message content.
==========


##########################################################################
# Missing newsgroups header                                            (9)

fail newsgroups: missing required Newsgroups header
newsgroups
newsgroups accept example.test
==========
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>

This is some message content.
==========


##########################################################################
# No accepted groups                                                  (10)

fail newsgroups: no accepted newsgroups in Newsgroups header
newsgroups
newsgroups accept example.test
==========
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>
Newsgroups: example.local

This is some message content.
==========


##########################################################################
# Not primary instance                                                (11)

fail newsgroups: not primary instance
newsgroups
newsgroups group example.config
newsgroups map example.test test@example.org
newsgroups map example.config config@example.com
newsgroups map example.local /.*@local\.example\.com$/
==========
Message-ID: <test@example.org>
To: Foo <test@example.org>, config@example.com (Bar)
Cc: <baz@local.example.com>

This is some message content.
==========
