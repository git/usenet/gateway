#!/usr/bin/perl
# previoushop.t -- Test for previoushop rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

2

##########################################################################
# Basic functionality                                                  (2)

pass
previoushop
==========
From: Russ Allbery <eagle@eyrie.org>
Received: from foobar (foobar.example.com [1.2.3.4]) by...
Path: news.example.com

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Received: from foobar (foobar.example.com [1.2.3.4]) by...
Path: foobar!news.example.com

This is some message content.
==========


##########################################################################
# No existing Path header                                              (3)

pass
previoushop
==========
From: Russ Allbery <eagle@eyrie.org>
Received: from blah.example.com by smtp.example.com via...

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Received: from blah.example.com by smtp.example.com via...
Path: blah.example.com

This is some message content.
==========
