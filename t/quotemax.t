#!/usr/bin/perl
# quotemax.t -- Test for quotemax rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
chdir 't' if (!-d 'data' && -d 't/data');
require "driver.pl";
test_rewrites (\*DATA);

__END__

3

##########################################################################
# Basic functionality                                                  (2)

fail quotemax: excessive quoting (4/5, 80.00% > 75.00%)
quotemax
quotemax limit 75
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Much quoting

> quoted
  > > > quoted


> : < quoted
>>> quoted


% not quoted
==========


##########################################################################
# Line limits                                                          (3)

pass
quotemax
quotemax limit 75
quotemax minlines 6
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Much quoting

> quoted
  > > > quoted


> : < quoted
>>> quoted


% not quoted
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Much quoting

> quoted
  > > > quoted


> : < quoted
>>> quoted


% not quoted
==========


##########################################################################
# Nonstandard quote regex                                              (4)

fail quotemax: excessive quoting (1/5, 20.00% > 0.00%)
quotemax
quotemax limit 0
quotemax minlines 5
quotemax regex ^%
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Much quoting

> not quoted
  > > > not quoted


> : < not quoted
>>> not quoted


% quoted
==========
