#!/usr/bin/perl
# stripheaders.t -- Test for stripheaders rewrite module.  -*- perl -*-

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

1

##########################################################################
# Basic functionality                                                  (2)

pass
stripheaders
==========
Return-Path: <bounce-debian-devel@lists.debian.org>
Delivered-To: eagle@eyrie.org
Received: (qmail 8872 invoked by uid 106); 17 May 2005 06:19:22 -0000
Received: from leland.stanford.edu (171.67.16.29)
  by windlord.stanford.edu with SMTP; 17 May 2005 06:19:22 -0000
Received: from murphy.debian.org (murphy.debian.org [146.82.138.6])
	by leland.Stanford.EDU (8.12.11/8.12.11) with ESMTP id j4H6JL5W025383
	for <eagle@eyrie.org>; Mon, 16 May 2005 23:19:21 -0700
Received: from localhost (localhost [127.0.0.1])
	by murphy.debian.org (Postfix) with QMQP
	id 7DAC72E30E; Tue, 17 May 2005 01:19:07 -0500 (CDT)
Old-Return-Path: <eagle@eyrie.org>
X-Original-To: debian-devel@lists.debian.org
Received: from smtp2.Stanford.EDU (smtp2.Stanford.EDU [171.67.16.125])
	by murphy.debian.org (Postfix) with ESMTP id 3C06C2DDC6
	for <debian-devel@lists.debian.org>; Tue, 17 May 2005 01:19:01 -0500 (CDT)
Received: from windlord.stanford.edu (windlord.Stanford.EDU [171.64.19.147])
	by smtp2.Stanford.EDU (8.12.11/8.12.11) with SMTP id j4H6IxAe021448
	for <debian-devel@lists.debian.org>; Mon, 16 May 2005 23:19:00 -0700
Received: (qmail 8847 invoked by uid 1000); 17 May 2005 06:18:59 -0000
To: debian-devel@lists.debian.org
Subject: Re: RES: /usr/lib vs /usr/libexec
In-Reply-To: <87ekc6zld7.fsf@becket.becket.net> (Thomas Bushnell's message
 of "Mon, 16 May 2005 18:34:12 -0700")
References: <D983A82B2EE1D211BED00008C7330586079F7D56@almg-nt9.almg.uucp>
	<7FC01D8D-840A-41E5-9E51-ACA76465F4B7@suespammers.org>
	<87psvqzmed.fsf@becket.becket.net>
	<87br7a640e.fsf@windlord.stanford.edu>
	<87ekc6zld7.fsf@becket.becket.net>
From: Russ Allbery <eagle@eyrie.org>
Organization: The Eyrie
Date: Mon May 16 23:18:59 2005 -0700
Message-ID: <8764xi4bos.fsf@windlord.stanford.edu>
User-Agent: Gnus/5.1007 (Gnus v5.10.7) XEmacs/21.4 (Jumbo Shrimp, linux)
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Resent-Message-ID: <pPj1YD.A.HoG.bzYiCB@murphy>
Resent-From: debian-devel@lists.debian.org
X-Loop: debian-devel@lists.debian.org
Precedence: list
Resent-Sender: debian-devel-request@lists.debian.org
Resent-Date: Tue, 17 May 2005 01:19:07 -0500 (CDT)
Lines: 48
Xref: windlord.stanford.edu lists.debian.devel:1336

This is some message content.
==========
Subject: Re: RES: /usr/lib vs /usr/libexec
In-Reply-To: <87ekc6zld7.fsf@becket.becket.net> (Thomas Bushnell's message
 of "Mon, 16 May 2005 18:34:12 -0700")
References: <D983A82B2EE1D211BED00008C7330586079F7D56@almg-nt9.almg.uucp>
	<7FC01D8D-840A-41E5-9E51-ACA76465F4B7@suespammers.org>
	<87psvqzmed.fsf@becket.becket.net>
	<87br7a640e.fsf@windlord.stanford.edu>
	<87ekc6zld7.fsf@becket.becket.net>
From: Russ Allbery <eagle@eyrie.org>
Organization: The Eyrie
Date: Mon May 16 23:18:59 2005 -0700
Message-ID: <8764xi4bos.fsf@windlord.stanford.edu>
User-Agent: Gnus/5.1007 (Gnus v5.10.7) XEmacs/21.4 (Jumbo Shrimp, linux)
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii

This is some message content.
==========
