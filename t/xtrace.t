#!/usr/bin/perl
# xtrace.t -- Test for xtrace rewrite module.  -*- perl -*-

# Load our driver module and run our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

6

##########################################################################
# Basic functionality                                                  (2)

pass
xtrace
==========
From: Russ Allbery <eagle@eyrie.org>
NNTP-posting-host: invalid.example.com
X-Trace: news.Stanford.EDU 1110572428 8550 10.10.10.10 (11 Mar 2005 20:20:28 GMT)
NNTP-Posting-User: example
NNTP-Posting-Date: Fri Mar 11 12:20:28 2005 -0800

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
X-Modtrace: example@invalid.example.com (11 Mar 2005 20:20:28 GMT)
X-Poster-Trace: news.Stanford.EDU 1110572428 8550 10.10.10.10 (11 Mar 2005 20:20:28 GMT)

This is some message content.
==========


##########################################################################
# Only the X-Trace header                                              (3)

pass
xtrace
==========
From: Russ Allbery <eagle@eyrie.org>
X-Trace: news.Stanford.EDU 1110572428 8550 10.10.10.10 (11 Mar 2005 20:20:28 GMT)

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
X-Modtrace: 10.10.10.10 (11 Mar 2005 20:20:28 GMT)
X-Poster-Trace: news.Stanford.EDU 1110572428 8550 10.10.10.10 (11 Mar 2005 20:20:28 GMT)

This is some message content.
==========


##########################################################################
# NNTP-Posting-User with host                                          (4)

pass
xtrace
==========
From: Russ Allbery <eagle@eyrie.org>
NNTP-Posting-User: example@example.org
NNTP-Posting-Host: example.org
Date: Fri Mar 11 12:20:28 2005 -0800

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Date: Fri Mar 11 12:20:28 2005 -0800
X-Modtrace: example@example.org (Fri Mar 11 12:20:28 2005 -0800)

This is some message content.
==========


##########################################################################
# Host mismatch, no date                                               (5)

pass
xtrace
==========
From: Russ Allbery <eagle@eyrie.org>
NNTP-Posting-User: example@example.org
NNTP-Posting-Host: invalid.example.org

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
X-Modtrace: example@example.org (invalid.example.org)

This is some message content.
==========


##########################################################################
# Ignore 127.0.0.1, different header                                   (6)

pass
xtrace
xtrace X-Gateway-Trace
==========
From: Russ Allbery <eagle@eyrie.org>
NNTP-Posting-User: example
X-Trace: news.Stanford.EDU 1110572428 8550 127.0.0.1 (11 Mar 2005 20:20:28 GMT)

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
X-Gateway-Trace: example@UNKNOWN (11 Mar 2005 20:20:28 GMT)
X-Poster-Trace: news.Stanford.EDU 1110572428 8550 127.0.0.1 (11 Mar 2005 20:20:28 GMT)

This is some message content.
==========


##########################################################################
# No data                                                              (7)

pass
xtrace
==========
From: Russ Allbery <eagle@eyrie.org>

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
X-Modtrace: UNKNOWN

This is some message content.
==========
