#!/usr/bin/perl
# wrap.t -- Test for wrap rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

3

##########################################################################
# Basic functionality                                                  (2)

pass
wrap
==========
From: Russ Allbery <eagle@eyrie.org>

This is an extremely long line that should be wrapped at the default line length but before the word length.
==========
From: Russ Allbery <eagle@eyrie.org>

This is an extremely long line that should be wrapped at the default line
length but before the word length.
==========

##########################################################################
# Configuration and quotes                                             (3)

pass
wrap
wrap limit 80
wrap length 40
==========
From: Russ Allbery <eagle@eyrie.org>

>>>  This is an extremely long line that should be wrapped at the default line length but before the word length.
==========
From: Russ Allbery <eagle@eyrie.org>

>>>  This is an extremely long line
>>>  that should be wrapped at the
>>>  default line length but before the word length.
==========

##########################################################################
# Different quote marks                                                (4)

pass
wrap
wrap quote "T"
==========
From: Russ Allbery <eagle@eyrie.org>

This is an extremely long line that should be wrapped at the default line length but before the word length.
==========
From: Russ Allbery <eagle@eyrie.org>

This is an extremely long line that should be wrapped at the default line
Tlength but before the word length.
==========
