#!/usr/bin/perl
# nobinaries.t -- Test for nobinaries rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

7

##########################################################################
# Bad encoding                                                         (2)

fail nobinaries: base64 encoded
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Transfer-Encoding: base64

This is some message content.
==========


##########################################################################
# Invalid content type                                                 (3)

fail nobinaries: invalid content type
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Type: application/octet-stream

This is some message content.
==========


##########################################################################
# Invalid content type in body                                         (4)

fail nobinaries: invalid content type
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>

stuff stuff stuff

Content-Type: image/jpeg

stuff stuff stuff
==========


##########################################################################
# Invalid transfer encoding in body                                    (5)

fail nobinaries: base64 encoded
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>

stuff stuff stuff

Content-transfer-encodinG:      base64

stuff stuff stuff
==========


##########################################################################
# uuencoded attachment                                                 (6)

fail nobinaries: apparently uuencoded
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>

begin 755 anykeyword.t
M(R$O=7-R+V)I;B]P97)L"B,@86YY:V5Y=V]R9"YT("TM(%1E<W0@9F]R(&%N
M>6ME>7=O<F0@<F5W<FET92!M;V1U;&4N"B,@)$ED.B!A;GEK97EW;W)D+G0L
M=B`P+C$@,3DY.2\P-B\P,R`P-3HP,3HS-R!E86=L92!%>'`@)`H*(R!,;V%D
M(&]U<B!D<FEV97(@;6]D=6QE(&%N9"!F:6YD(&]U<B!T97-T<RX*)'P@/2`Q
M.PIU<V4@;&EB('%W*'0@+BXO="!B;&EB+VQI8B`N+B]B;&EB+VQI8BD["G)E
M<75I<F4@(F1R:79E<BYP;"(["G1E<W1?<F5W<FET97,@*%PJ1$%402D["@I?
M7T5.1%]?"@HT"@HC(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C
M(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(PHC($)A
M<VEC(&9U;F-T:6]N86QI='D@("`@("`@("`@("`@("`@("`@("`@("`@("`@
M("`@("`@("`@("`@("`@("`@("`@("@R*0H*<&%S<PIA;GEK97EW;W)D"CT]
M/3T]/3T]/3T*1G)O;3H@4G5S<R!!;&QB97)Y(#QR<F%`<W1A;F9O<F0N961U
M/@I3=6)J96-T.B!;345405T@36]N=&AL>2!&05$@<&]S=&EN9PH*5&AI<R!I
M<R!S;VUE(&UE<W-A9V4@8V]N=&5N="X*/3T]/3T]/3T]/0I&<F]M.B!2=7-S
M($%L;&)E<GD@/')R84!S=&%N9F]R9"YE9'4^"E-U8FIE8W0Z(%M-151!72!-
M;VYT:&QY($9!42!P;W-T:6YG"@I4:&ES(&ES('-O;64@;65S<V%G92!C;VYT
M96YT+@H]/3T]/3T]/3T]"@H*(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C
M(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C
M(R,*(R!!;&QO=R!R97!L:65S("`@("`@("`@("`@("`@("`@("`@("`@("`@
M("`@("`@("`@("`@("`@("`@("`@("`@("`@("`H,RD*"G!A<W,*86YY:V5Y
M=V]R9`H]/3T]/3T]/3T]"D9R;VTZ(%)U<W,@06QL8F5R>2`\<G)A0'-T86YF
M;W)D+F5D=3X*4W5B:F5C=#H@4F4Z(%M-151!72!-;VYT:&QY($9!42!P;W-T
M:6YG"@I4:&ES(&ES('-O;64@;65S<V%G92!C;VYT96YT+@H]/3T]/3T]/3T]
M"D9R;VTZ(%)U<W,@06QL8F5R>2`\<G)A0'-T86YF;W)D+F5D=3X*4W5B:F5C
M=#H@4F4Z(%M-151!72!-;VYT:&QY($9!42!P;W-T:6YG"@I4:&ES(&ES('-O
M;64@;65S<V%G92!C;VYT96YT+@H]/3T]/3T]/3T]"@H*(R,C(R,C(R,C(R,C
M(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C
M(R,C(R,C(R,C(R,C(R,C(R,*(R!296IE8W0@87)T:6-L92!W:71H;W5T(&$@
M:V5Y=V]R9"`@("`@("`@("`@("`@("`@("`@("`@("`@("`@("`@("`@("`H
M-"D*"F9A:6P@86YY:V5Y=V]R9#H@3F\@:V5Y=V]R9"!F;W5N9`IA;GEK97EW
M;W)D"CT]/3T]/3T]/3T*1G)O;3H@4G5S<R!!;&QB97)Y(#QR<F%`<W1A;F9O
M<F0N961U/@I3=6)J96-T.B!293H@36]N=&AL>2!&05$@<&]S=&EN9PH*5&AI
M<R!I<R!S;VUE(&UE<W-A9V4@8V]N=&5N="X*/3T]/3T]/3T]/0H*"B,C(R,C
M(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C
M(R,C(R,C(R,C(R,C(R,C(R,C(R,C(R,C"B,@4F5J96-T(&%R=&EC;&4@=VET
M:&]U="!A(%-U8FIE8W0@:&5A9&5R("`@("`@("`@("`@("`@("`@("`@("`@
M("`@("`@*#4I"@IF86EL(&%N>6ME>7=O<F0Z($YO(&ME>7=O<F0@9F]U;F0*
M86YY:V5Y=V]R9`H]/3T]/3T]/3T]"D9R;VTZ(%)U<W,@06QL8F5R>2`\<G)A
M0'-T86YF;W)D+F5D=3X*"E1H:7,@:7,@<V]M92!M97-S86=E(&-O;G1E;G0N
,"CT]/3T]/3T]/3T*
`
end
==========


##########################################################################
# base64-encoded attachment                                            (7)

fail nobinaries: apparently base64-encoded
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>

IyEvdXNyL2Jpbi9wZXJsCiMgaGVhZGVyLnQgLS0gVGVzdCBmb3IgaGVhZGVyIHJld3JpdGUg
bW9kdWxlLiAgLSotIHBlcmwgLSotCiMgJElkOiBoZWFkZXIudCx2IDAuMSAxOTk5LzA2LzAy
IDIzOjQzOjIyIGVhZ2xlIEV4cCAkCgojIExvYWQgb3VyIGRyaXZlciBtb2R1bGUgYW5kIGZp
bmQgb3VyIHRlc3RzLgokfCA9IDE7CnVzZSBsaWIgcXcodCAuLi90IGJsaWIvbGliIC4uL2Js
aWIvbGliKTsKcmVxdWlyZSAiZHJpdmVyLnBsIjsKdGVzdF9yZXdyaXRlcyAoXCpEQVRBKTsK
Cl9fRU5EX18KCjMKCiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMj
IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjCiMgQmFzaWMgZnVuY3Rpb25hbGl0
eSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKDIp
CgpwYXNzCmhlYWRlcgpoZWFkZXIgbWVzc2FnZS1pZCAgIHJlbmFtZQpoZWFkZXIgc2VuZGVy
ICAgICAgIGRyb3AKaGVhZGVyIG9yZ2FuaXphdGlvbiBhZGQgICAgICAiU05BUCIKaGVhZGVy
IGNvbW1lbnQgICAgICByZXBsYWNlICBnYXRld2F5IDAuNApoZWFkZXIgc3ViamVjdCAgICAg
IGlmZW1wdHkgIG5vIHN1YmplY3QgICh0aHJlYWQgYmxhaCkKaGVhZGVyIHBhdGggICAgICAg
ICBwcmVwZW5kICBtYWlsdG9uZXdzISAgCj09PT09PT09PT0KUGF0aDogbmV3cy5leGFtcGxl
LmNvbQpPcmdhbml6YXRpb246IFJlc3RhdXJhbnQgUmV2aWV3cwpNZXNzYWdlLUlEOiA8MTIz
MTQyQGJhci5vcmc+ClNlbmRlcjogZm9vQGJhci5vcmcKQ29tbWVudDogSGVsbG8KQ29tbWVu
dDogSGVsbG8gYWdhaW4KClRoaXMgaXMgc29tZSBtZXNzYWdlIGNvbnRlbnQuCj09PT09PT09
PT0KUGF0aDogbWFpbHRvbmV3cyFuZXdzLmV4YW1wbGUuY29tCk9yZ2FuaXphdGlvbjogUmVz
dGF1cmFudCBSZXZpZXdzCkNvbW1lbnQ6IGdhdGV3YXkgMC40Ck9yZ2FuaXphdGlvbjogU05B
UApTdWJqZWN0OiBubyBzdWJqZWN0ICh0aHJlYWQgYmxhaCkKWC1PcmlnaW5hbC1NZXNzYWdl
LUlEOiA8MTIzMTQyQGJhci5vcmc+CgpUaGlzIGlzIHNvbWUgbWVzc2FnZSBjb250ZW50Lgo9
PT09PT09PT09CgoKIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMj
IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMKIyBCYXNpYyBmdW5jdGlvbmFsaXR5
LCBwYXJ0IHR3byAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoMykK
CnBhc3MKaGVhZGVyCmhlYWRlciBwYXRoICAgICAgIHByZXBlbmQgIG1haWx0b25ld3MhCmhl
YWRlciBzdWJqZWN0ICAgIGlmZW1wdHkgIG5vIHN1YmplY3QgKHRocmVhZCBibGFoKQpoZWFk
ZXIgeC1jb21tZW50ICBhZGQgICAgICAic3R1ZmYgd2l0aCAgZW1iZWRkZWQgIHNwYWNlcyAg
Igo9PT09PT09PT09CkZyb206IFJ1c3MgQWxsYmVyeSA8cnJhQHN0YW5mb3JkLmVkdT4KU3Vi
amVjdDogUmVzdGF1cmFudCBSZXZpZXdzCk1lc3NhZ2UtSUQ6IDwxMjMxNDJAYmFyLm9yZz4K
U2VuZGVyOiBmb29AYmFyLm9yZwpDb21tZW50OiBIZWxsbwpDb21tZW50OiBIZWxsbyBhZ2Fp
bgoKVGhpcyBpcyBzb21lIG1lc3NhZ2UgY29udGVudC4KPT09PT09PT09PQpGcm9tOiBSdXNz
IEFsbGJlcnkgPHJyYUBzdGFuZm9yZC5lZHU+ClN1YmplY3Q6IFJlc3RhdXJhbnQgUmV2aWV3
cwpNZXNzYWdlLUlEOiA8MTIzMTQyQGJhci5vcmc+ClNlbmRlcjogZm9vQGJhci5vcmcKQ29t
bWVudDogSGVsbG8KQ29tbWVudDogSGVsbG8gYWdhaW4KUGF0aDogbWFpbHRvbmV3cyEKWC1D
b21tZW50OiBzdHVmZiB3aXRoICBlbWJlZGRlZCAgc3BhY2VzICAKClRoaXMgaXMgc29tZSBt
ZXNzYWdlIGNvbnRlbnQuCj09PT09PT09PT0KCgojIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMj
IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIwojIFJl
amVjdGluZyBoZWFkZXJzICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICg0KQoKZmFpbCBoZWFkZXI6IEludmFsaWQgaGVhZGVyIHgtaW52YWxp
ZApoZWFkZXIKaGVhZGVyIHgtSW52YWxpZCByZWplY3QKPT09PT09PT09PQpGcm9tOiBSdXNz
IEFsbGJlcnkgPHJyYUBzdGFuZm9yZC5lZHU+ClRvOiBnYXRld2F5QGV4YW1wbGUuY29tClN1
YmplY3Q6IFN0dWZmClgtaW52YWxpZDogc29tZSBoZWFkZXIKClRoaXMgaXMgc29tZSBtZXNz
YWdlIGNvbnRlbnQuCj09PT09PT09PT0K
==========

##########################################################################
# No false positives                                                   (8)

pass
nobinaries
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========
