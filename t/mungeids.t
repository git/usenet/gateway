#!/usr/bin/perl
# mungeids.t -- Test for mungeids rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

5

##########################################################################
# Basic functionality                                                  (2)

pass
mungeids
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <vatr$1523@example.com>
Newsgroups: local.test

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <local.test/vatr$1523@example.com>
Newsgroups: local.test

This is some message content.
==========


##########################################################################
# Strip anything that looks like a prefix                              (3)

pass
mungeids
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.announce/vatr$1523@example.com>
Newsgroups: local.test

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <local.test/vatr$1523@example.com>
Newsgroups: local.test

This is some message content.
==========


##########################################################################
# Multiple newsgroups                                                  (4)

pass
mungeids
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.announce/example.test/local/vatr$1523@example.com>
Newsgroups: local.test,example.junk

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.junk/local.test/local/vatr$1523@example.com>
Newsgroups: local.test,example.junk

This is some message content.
==========


##########################################################################
# Configurable regexes                                                 (5)

pass
mungeids
mungeids /^example\./
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.announce/example.test/local/vatr$1523@example.com>
Newsgroups: local.test,example.junk

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.junk/local/vatr$1523@example.com>
Newsgroups: local.test,example.junk

This is some message content.
==========


##########################################################################
# References refolding                                                 (6)

pass
mungeids
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.announce/example.test/local/vatr$1523@example.com>
References: <example.local/bagasf$134@example.com> <foo$bar@example.com>
  <baz$bar@example.com>
Newsgroups: local.test,example.junk

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Message-ID: <example.junk/local.test/local/vatr$1523@example.com>
References: <example.junk/local.test/bagasf$134@example.com>
	<example.junk/local.test/foo$bar@example.com>
	<example.junk/local.test/baz$bar@example.com>
Newsgroups: local.test,example.junk

This is some message content.
==========
