#!/usr/bin/perl
# header.t -- Test for header rewrite module.  -*- perl -*-

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
chdir 't' if (!-d 'data' && -d 't/data');
test_rewrites (\*DATA);

__END__

5

##########################################################################
# Basic functionality                                                  (2)

pass
header
header rename   message-id
header drop     sender
header add      organization "SNAP"
header replace  comment      gateway 0.4
header ifempty  subject      no subject  (thread blah)
header prepend  path         mailtonews!  
==========
Path: news.example.com
Organization: Restaurant Reviews
Message-ID: <123142@bar.org>
Sender: foo@bar.org
Comment: Hello
Comment: Hello again

This is some message content.
==========
Path: mailtonews!news.example.com
Organization: Restaurant Reviews
Comment: gateway 0.4
Organization: SNAP
Subject: no subject (thread blah)
X-Original-Message-ID: <123142@bar.org>

This is some message content.
==========


##########################################################################
# Basic functionality, part two                                        (3)

pass
header
header prepend  path       mailtonews!
header ifempty  subject    no subject (thread blah)
header add      x-comment  "stuff with  embedded  spaces  "
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Restaurant Reviews
Message-ID: <123142@bar.org>
Sender: foo@bar.org
Comment: Hello
Comment: Hello again

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Restaurant Reviews
Message-ID: <123142@bar.org>
Sender: foo@bar.org
Comment: Hello
Comment: Hello again
Path: mailtonews!
X-Comment: stuff with  embedded  spaces  

This is some message content.
==========


##########################################################################
# Rejecting headers                                                    (4)

fail header: invalid header x-invalid
header
header reject x-Invalid
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject: Stuff
X-invalid: some header

This is some message content.
==========


##########################################################################
# Add if from success                                                  (5)

pass
header
header add-if-from data/whitelist Pass yes
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Pass: yes

This is some message content.
==========


##########################################################################
# Add if from fail                                                     (6)

pass
header
header add-if-from data/whitelist Pass yes
==========
From: Russ Allbery <rra@example.com>
To: gateway@example.com

This is some message content.
==========
From: Russ Allbery <rra@example.com>
To: gateway@example.com

This is some message content.
==========
