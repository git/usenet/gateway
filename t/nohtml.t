#!/usr/bin/perl
# nohtml.t -- Test for nohtml rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

7

##########################################################################
# Bad Content-Type                                                     (2)

fail nohtml: HTML message
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Type:  text/html ; charset=iso8859-1

Body doesn't matter in this case.
==========


##########################################################################
# Basic HTML message                                                   (3)

fail nohtml: probable HTML message
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>

<html><body><p>Stupid syntax.</p></body></html>
==========


##########################################################################
# URLs shouldn't count                                                 (4)

pass
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>

<URL:http://www.eyrie.org/~eagle/>
==========
From: Russ Allbery <eagle@eyrie.org>

<URL:http://www.eyrie.org/~eagle/>
==========

##########################################################################
# E-mail addresses shouldn't count either                              (5)

pass
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>

Russ Allbery <eagle@eyrie.org>
==========
From: Russ Allbery <eagle@eyrie.org>

Russ Allbery <eagle@eyrie.org>
==========


##########################################################################
# Nor should URLs without the <URL: part                               (6)

pass
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>

<http://www.eyrie.org/~eagle/>
==========
From: Russ Allbery <eagle@eyrie.org>

<http://www.eyrie.org/~eagle/>
==========

##########################################################################
# If there's enough non-HTML, we should pass                           (7)

pass
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>

This is some text
this is more text
<a href="foo:bar">Now some HTML</a>
And some <bold>more</bold> HTML.
But here is more text
and more text.
So we should be fine.
==========
From: Russ Allbery <eagle@eyrie.org>

This is some text
this is more text
<a href="foo:bar">Now some HTML</a>
And some <bold>more</bold> HTML.
But here is more text
and more text.
So we should be fine.
==========


##########################################################################
# But <html> is really bad                                             (8)

fail nohtml: probable HTML message
nohtml
==========
From: Russ Allbery <eagle@eyrie.org>

<html><pre>
This is text
and some more text
and even more text
But it doesn't matter
even though there are more lines
than HTML tags
because the html tag
is particularly bad
and we weigh it heavily.
</pre></html>
==========
