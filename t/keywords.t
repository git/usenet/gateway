#!/usr/bin/perl
# keywords.t -- Test for keywords rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
chdir 't' if (!-d 'data' && -d 't/data');
require "driver.pl";
test_rewrites (\*DATA);

__END__

4

##########################################################################
# Basic functionality                                                  (2)

pass
keywords
keywords valid meta
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Multiple keywords from a file                                        (3)

pass
keywords
keywords valid foo
keywords file data/keywords
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: [META][FOO] Monthly FAQ posting

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: [META][FOO] Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Reject article without a keyword                                     (4)

fail keywords: no keywords found
keywords
keywords valid meta
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Reject article with an invalid keyword                               (5)

fail keywords: invalid keyword: BAR
keywords
keywords valid meta
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: META/BAR: Stuff

This is some message content.
==========
