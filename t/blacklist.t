#!/usr/bin/perl
# blacklist.t -- Test for blacklist rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
chdir 't' if (!-d 'data' && -d 't/data');
require "driver.pl";
test_rewrites (\*DATA);

__END__

3

##########################################################################
# Basic functionality                                                  (2)

pass
blacklist
blacklist from data/blacklist.from
blacklist subject data/blacklist.subject
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ testing

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ testing

This is some message content.
==========


##########################################################################
# Blacklisted From header                                              (3)

fail blacklist: from is on blacklist
blacklist
blacklist from data/blacklist.from
blacklist subject data/blacklist.subject
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Blacklisted Subject header                                           (4)

fail blacklist: subject is on blacklist
blacklist
blacklist from data/blacklist.from
blacklist subject data/blacklist.subject
==========
From: eagle@eyrie.org (Russ Allbery)
Subject: This might be a test

This is some message content.
==========
