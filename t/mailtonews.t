#!/usr/bin/perl
# mailtonews.t -- Test for mailtonews rewrite module.  -*- perl -*-

# Load our driver module and run our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

8

##########################################################################
# Basic functionality                                                  (2)

pass
mailtonews
==========
From: Russ Allbery
 <eagle@eyrie.org>
Subject: Test mailtonews message
To: gateway@example.com
Newsgroups: invalid.test, invalid.stuff ,,invalid.bar,, invalid.test
Date-Received: Sat Apr  3 16:38:54 PST 1999
posted: Sat Apr  3 16:28:34 PST 1999
recEived: (stuff)
Relay-version: 5.6
Xref: invalid.test:193
Lines: 34
NNTP-posting-host: invalid.example.com
Message-id: <foo@bar>
References: <1.2.3@com.invalid>
In-Reply-To: The message <4.01.1@example.com> by Someone Important

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test mailtonews message
To: gateway@example.com
Newsgroups: invalid.test,invalid.stuff,invalid.bar
References: <1.2.3@com.invalid> <4.01.1@example.com>
In-Reply-To: The message <4.01.1@example.com> by Someone Important
X-Original-Message-ID: <foo@bar>
X-Original-NNTP-Posting-Host: invalid.example.com

This is some message content.
==========


##########################################################################
# From header required                                                 (3)

fail mailtonews: missing required From header
mailtonews
==========
Subject: Test mailtonews message
To: gateway@example.com

This is some message content.
==========


##########################################################################
# Non-empty body required                                              (4)

fail mailtonews: empty body
mailtonews
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test mailtonews message
To: gateway@example.com

==========


##########################################################################
# Empty subject handling                                               (5)

pass
mailtonews
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Message-ID: <valid@windlord.stanford.edu>

Some body content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Message-ID: <valid@windlord.stanford.edu>
Subject: (none)

Some body content.
==========


##########################################################################
# Subject unfolding, bad message ID handling                           (6)

pass
mailtonews
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject:   
  Fwd: Stuff  
Message-ID: <valid@windlord.stanford.edu.>

Some body content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject: Fwd: Stuff  
X-Original-Message-ID: <valid@windlord.stanford.edu.>

Some body content.
==========


##########################################################################
# In-Reply-To message ID lifting with duplicates                       (7)

pass
mailtonews
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject:   
  Fwd: Stuff  
References: <valid@windlord.stanford.edu>
In-Reply-To: Response to the message <valid@windlord.stanford.edu>

Some body content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject: Fwd: Stuff  
References: <valid@windlord.stanford.edu>
In-Reply-To: Response to the message <valid@windlord.stanford.edu>

Some body content.
==========


##########################################################################
# Trace header handling                                                (8)

pass
mailtonews
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
NNTP-Posting-Host: windlord.stanford.edu
NNTP-Posting-User: rra
X-Trace: windlord.stanford.edu 1110298268 29755 127.0.0.1 (8 Mar 2005 16:11:08 GMT)
Path: not-for-mail
NNTP-Posting-Date: Tue Mar  8 08:11:07 2005 -0800
Injector-Info: I think this header is stupid
Sender: eagle@eyrie.org
X-Complaints-To: news@news.stanford.edu
Subject: foo

Some body content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject: foo
X-Original-Injector-Info: I think this header is stupid
X-Original-NNTP-Posting-Date: Tue Mar  8 08:11:07 2005 -0800
X-Original-NNTP-Posting-Host: windlord.stanford.edu
X-Original-NNTP-Posting-User: rra
X-Original-Sender: eagle@eyrie.org
X-Poster-Complaints-To: news@news.stanford.edu
X-Poster-Trace: windlord.stanford.edu 1110298268 29755 127.0.0.1 (8 Mar 2005 16:11:08 GMT)

Some body content.
==========


##########################################################################
# More bad message IDs, valuable Path header                           (9)

pass
mailtonews
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Path: windlord.stanford.edu!not-for-mail
Subject: Stuff
Message-ID: <not@valid@windlord.stanford.edu>

Some body content.
==========
From: Russ Allbery <eagle@eyrie.org>
To: gateway@example.com
Subject: Stuff
X-Original-Message-ID: <not@valid@windlord.stanford.edu>
X-Original-Path: windlord.stanford.edu!not-for-mail

Some body content.
==========
