#!/usr/bin/perl
# clean.t -- Test for clean rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

1

##########################################################################
# Basic functionality                                                  (2)

pass
clean
clean maxrefs 8
clean hostname example.com
clean mid-prefix foo
==========
Newsgroups: example.test,,example.bar,example.test
Followup-To: example.foo,poster
References: <1@example.com> <2@example.com> <3@example.com>
 <4@example.com> <5@example.com> <6@example.com> <7@example.com>
 <8@example.com> <9@example.com> <10@example.com> <11@example.com>
Date: Sat Mar 25 20:29:28 PST 2006
Subject: Re: Re: This is a test
Message-ID: <foobar@example.com>

This is a test.
==========
Newsgroups: example.bar,example.test
Followup-To: poster
References: <1@example.com> <trimmed-3@example.com> <5@example.com>
	<6@example.com> <7@example.com> <8@example.com> <9@example.com>
	<10@example.com> <11@example.com>
Date: Sat, 25 Mar 2006 20:29:28 -0800
Subject: Re: This is a test
Message-ID: <foobar@example.com>

This is a test.
==========
