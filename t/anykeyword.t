#!/usr/bin/perl
# anykeyword.t -- Test for anykeyword rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

4

##########################################################################
# Basic functionality                                                  (2)

pass
anykeyword
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Allow replies                                                        (3)

pass
anykeyword
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: [META] Monthly FAQ posting

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: [META] Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Reject article without a keyword                                     (4)

fail anykeyword: no keyword found
anykeyword
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Reject article without a Subject header                              (5)

fail anykeyword: no keyword found
anykeyword
==========
From: Russ Allbery <eagle@eyrie.org>

This is some message content.
==========
