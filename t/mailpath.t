#!/usr/bin/perl
# mailpath.t -- Test for mailpath rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

1

##########################################################################
# Basic functionality                                                  (2)

pass
mailpath header
header drop received
==========
From eagle@eyrie.org Wed May 26 02:32:12 1999
From: Russ Allbery <eagle@eyrie.org>
Received: from foobar (really [1.2.3.4]) by...
Received: from foobar (foobar.baz.com [1.2.3.4]) by...
Received: from foobar (actually foobar.baz.com) by...
Received: from foobar ([1.2.3.4]) by...
Received: from foobar by...
          (peer crosschecked as: foobar.baz.com [1.2.3.4])
Received: from foobar by...
          (peer crosschecked as: [1.2.3.4])
Received: from foobar.baz.com (1.2.3.4) by...
Received: from foobar.baz.com (@1.2.3.4) by...
Received: from foobar.baz.com (something@1.2.3.4) by...
Received: from foobar.baz.com (HELO foobar) (1.2.3.4) by...
Received: from unknown (HELO foobar) (1.2.3.4) by...
Received: from foobar.baz.com [1.2.3.4] by...
Received: from foobar by...

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
X-Mail-Path: foobar[1.2.3.4]!foobar.baz.com!foobar.baz.com!foobar[1.2.3.4]!foobar.baz.com!foobar[1.2.3.4]!foobar.baz.com!foobar.baz.com!something@foobar.baz.com!foobar.baz.com!foobar[1.2.3.4]!foobar.baz.com[1.2.3.4]!foobar[UNTRUSTED]!eagle@eyrie.org

This is some message content.
==========
