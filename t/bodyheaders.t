#!/usr/bin/perl
# bodyheaders.t -- Test for bodyheaders rewrite module.  -*- perl -*-

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

3

##########################################################################
# Basic functionality                                                  (2)

pass
bodyheaders
bodyheaders x-no-archive comment
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test bodyheader message
To: gateway@example.com

X-No-Archive: yes

Comment: blah

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test bodyheader message
To: gateway@example.com
Comment: blah
X-No-Archive: yes

This is some message content.
==========


##########################################################################
# Skip unregistered headers                                            (3)

pass
bodyheaders
bodyheaders x-no-archive
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test bodyheader message
To: gateway@example.com

X-No-Archive: yes

Comment: blah

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test bodyheader message
To: gateway@example.com
X-No-Archive: yes

Comment: blah

This is some message content.
==========


##########################################################################
# Spacing and flush-margin tests                                       (4)

pass
bodyheaders
bodyheaders x-no-archive comment
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test bodyheader message
To: gateway@example.com

x-no-archive:yes
 Comment: blah

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Test bodyheader message
To: gateway@example.com
X-No-Archive: yes

 Comment: blah

This is some message content.
==========
