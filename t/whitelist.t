#!/usr/bin/perl
# whitelist.t -- Test for whitelist rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
chdir 't' if (!-d 'data' && -d 't/data');
require "driver.pl";
test_rewrites (\*DATA);

__END__

4

##########################################################################
# Basic functionality                                                  (2)

pass
whitelist
whitelist data/whitelist
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: [META] Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Reject article from unknown sender                                   (3)

fail whitelist: unknown poster eagle@eyrie.org
whitelist
whitelist data/whitelist
==========
From: Russ Allbery <eagle@eyrie.org>
Subject: Re: Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Different address format                                             (4)

pass
whitelist
whitelist data/whitelist
==========
From: eagle@eyrie.org (Russ Allbery)
Subject: [META] Monthly FAQ posting

This is some message content.
==========
From: eagle@eyrie.org (Russ Allbery)
Subject: [META] Monthly FAQ posting

This is some message content.
==========


##########################################################################
# Only an address                                                      (5)

pass
whitelist
whitelist data/whitelist
==========
From: eagle@eyrie.org
Subject: META/BAR: Stuff

This is some message content.
==========
From: eagle@eyrie.org
Subject: META/BAR: Stuff

This is some message content.
==========
