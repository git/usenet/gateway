#!/usr/bin/perl
# cleanbody.t -- Test for cleanbody rewrite module.

# Load our driver module and find our tests.
$| = 1;
use lib qw(t ../t blib/lib ../blib/lib);
require "driver.pl";
test_rewrites (\*DATA);

__END__

4

##########################################################################
# Basic functionality                                                  (2)

pass
cleanbody
==========
From: Russ Allbery <eagle@eyrie.org>



==========
From: Russ Allbery <eagle@eyrie.org>

`'""
----...TM
==========


##########################################################################
# Quoted-printable                                                     (3)

pass
cleanbody
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Transfer-Encoding: quoted-printable

x=3Dy =
y=3Dz =
z=3Dx
na=EFve
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Transfer-Encoding: 8bit

x=y y=z z=x
naοve
==========


##########################################################################
# Too long of a line                                                   (4)

fail cleanbody: line over 80 characters (line 2)
cleanbody
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Transfer-Encoding: quoted-printable

Stuff stuff stuff stuff this line isn't too long but it's right on the edge blah
Stuff stuff stuff stuff this line isn't too long but =
after combining it with the next line it will be too long.
Stuff.
==========


##########################################################################
# Invalid characters                                                   (5)

fail cleanbody: invalid characters in body (line 2)
cleanbody
==========
From: Russ Allbery <eagle@eyrie.org>
MIME-Version: 1.0
Content-Transfer-Encoding: quoted-printable

stuff =
stuff
Invalid  character
==========
