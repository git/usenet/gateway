$News::Gateway::HOOKS{rewriteid} = ['iddomain'];

    $gw->modules(
        'rewriteid'     => [$gw->datadir],
...
    );

sub News::Gateway::rewriteid_conf {
    my ($self, $directive, $domain) = @_;

    if ($directive eq 'iddomain') {
        $self->{rewriteid}{iddomain} = $domain;
    }
}

use BerkeleyDB;

sub News::Gateway::rewriteid_init {
    my ($self, $dir) = @_;

    return if $::ID_N2M_DB;

    my $env = BerkeleyDB::Env->new(
        -Flags => DB_CREATE|DB_INIT_MPOOL|DB_INIT_CDB,
        -Home => "$dir/db",
        -ErrFile => "$dir/db/err.log",
        -Verbose => 1|2|4|8,
    );
    die "Cannot create DB environment: $BerkeleyDB::Error" if not defined $env;

    $::ID_M2N_DB = BerkeleyDB::Hash->new(
        -Env => $env,
        -Flags => DB_CREATE,
        -Filename => 'id_m2n.db',
        -Pagesize => 512,
    );
    die "Cannot open DB id_m2n.db: $BerkeleyDB::Error"
        if not defined $::ID_M2N_DB;

    $::ID_N2M_DB = BerkeleyDB::Hash->new(
        -Env => $env,
        -Flags => DB_CREATE,
        -Filename => 'id_n2m.db',
        -Pagesize => 512,
    );
    die "Cannot open DB id_n2m.db: $BerkeleyDB::Error"
        if not defined $::ID_N2M_DB;
}

sub News::Gateway::rewriteid_mesg {
    my $self = $_[0];
    my $article = $self->{article};

    # from mailtonews.al
    my $rid = $article->header('In-Reply-To');
    if ($rid and $rid =~ /(<[^\s\@>]+\@[^\s.>]+(\.[^.\s>]+)*>)/) {
        $rid = $1;
        my $references = $article->header('References');
        if (not $references) {
            $article->set_headers(References => $rid);
        } elsif ((split(' ', $references))[-1] ne $rid) {
            $references .= ' ' . $rid;
            $article->set_headers(References => $references);
        }
        $article->drop_headers('In-Reply-To');
    }

    my $domain = $self->{rewriteid}{iddomain};
    my $id = $article->header('Message-ID');
    $article->set_headers(
        'Message-ID' => idconvert($id, $domain),
        'X-Original-Message-ID' => $id || '(none)',
    );

    my $ref = $article->header('References');
    if ($ref) {
        my $newref = '';
        foreach my $refid (split(/\s+/, $ref)) {
            $newref .= ' ' if $newref;
            $newref .= idconvert($refid, $domain);
        }
        $article->set_headers(
            'References' => $newref,
            'X-Original-References' => $ref,
        );
    }

    undef;
}

sub idconvert {
    my ($id, $domain) = @_;

    if (not $id) {                  # the article has no Message-ID
        return gen_message_id($domain);
    }

    my $val;
    my $r = $::ID_M2N_DB->db_get($id, $val);
    die $r if $r and $r !~ /^DB_NOTFOUND/;

    if ($r) {   # not found => ID not seen yet
        my $newid = gen_message_id($domain);
        $r = $::ID_M2N_DB->db_put($id, $newid . ' ' . time, DB_NOOVERWRITE);
        die "$r ($id, $newid ...)" if $r;
        $r = $::ID_N2M_DB->db_put($newid, $id, DB_NOOVERWRITE);
        die "$r ($newid, $id)" if $r;
        return $newid;
    } else {    # already processed ID
        return (split(/ /, $val))[0];
    }
}

sub gen_message_id {
    my ($domain, $prefix) = @_;
    my $offset = 1056000000;
    $prefix ||= '';

    $UniqueCounter++;
    return "<$prefix" . dec2k(time - $offset, 62) .  '-' . dec2k($$, 62)
        . '-' . $UniqueCounter++ . '@' . "$domain>";
}

sub dec2k {
    my ($num, $base, $symbols) = @_;
    $symbols ||= '0123456789' .
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $base ||= length $symbols;

    use integer;

    my $ret = '';
    while ($num != 0) {
        $ret .= substr($symbols, $num % $base, 1);
        $num /= $base;
    }

    return reverse $ret;
}
