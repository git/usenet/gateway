# crea dei nuovi moduli ######################################################
$News::Gateway::HOOKS{pgpverify}=['pgpverify'];
sub News::Gateway::pgpverify_conf {
	my ($self, $directive, $a, $b) = @_;
	if ($a eq 'mustsign') {
		$$self{pgpverify}{mustsign}=1;
	} else {
		open(PGPVERIFY, $a)
			or $self->error("Can't open authorized keys file $a: $!");
		local $_;
		while (<PGPVERIFY>) {
			chomp;
			push @{$self{pgpverify}{list}}, $_;
		}
		close PGPVERIFY;
	}
}

sub News::Gateway::pgpverify_mesg {
	my $self=shift;
	my $article=$$self{article};

	my $e=$article->verify_body(Homedir=>$GCNPath.'/gruppi/gnupg/');
	return "No signature found (or system error?)\n$e" if ($e);
	my $signer=$article->signer;
	foreach (@{$self{pgpverify}{list}}) {
		return undef if ($signer =~ /$_/i);
	}
	return 'User not authorized: '.$signer;
}

$News::Gateway::HOOKS{gcn}=[];
sub News::Gateway::gcn_mesg {
	my $self=shift;
	my $article=$$self{article};

	my $type=$article->header('content-type');
	return 'html'
		if ($type and ($type eq 'multipart/alternative' or $type =~ /html/i));

	my $body=$article->body;
	if (scalar @$body == 0) { return 'empty' }
	if (scalar @$body > 20) { # controlla il quoting e le dimensioni
		my $qlnum=0; my $artsize=0;
		foreach (@$body) {
			$qlnum++ if ((/^\s*[:%\>]]/) or (/^\s*\w{0,9}>/));
			$artsize+=length;
		}
		return 'quoting' if ( ($qlnum/(scalar @$body+1)) > 0.6 );
		return 'size' if ($artsize > 1000000);
	}

	undef;
}

$News::Gateway::HOOKS{footer}=[];
sub News::Gateway::footer_mesg {
	my $self=shift;
	my $body=$$self{article}->body;

	push @$body, @{$GroupData{footer}} if (defined @{$GroupData{footer}});
	undef;
}

sub News::Gateway::moderator {
	return $_[0]->{'moderator'} if (not defined $_[1]);
	$_[0]->{'moderator'}=$_[1];
}


# Estensione a News::Article
# es: $art->pgp_sign(Key=> "0x3C620D2D", Password => "x");
sub News::Article::sign_body {
	my ($self, %args)=@_;

	require '/home/md/projects/pgp/GPG.pm';
	my $pgp=new GPG;
	my @s=$pgp->Sign(Data => [join("\n", $self->body)], %args);
	return $pgp->Errors if (not defined @s);
	foreach (@s) { s/\r$//; }	# remove trailing ^Ms

	$self->set_body(@s);
	return ();
}

sub News::Article::verify_body {
	my ($self, %args)=@_;
	
	require '/home/md/projects/pgp/GPG.pm';
	$self->{signer}=undef;
	my $pgp=new GPG;
	my ($a, $b, $c, $d)=$pgp->Verify(Data => [join("\n", $self->body)], %args);
#print STDERR "-$a-$b-$c-$d-".$pgp->Errors."-\n";
	return $pgp->Errors if (not defined $a);
	return $pgp->Errors if ($b eq 'ERRSIG');
	return 'Bad signature from '.$c if $a;
	$self->{signer}=$c;
	if (defined $args{RemoveSig}) {
		$self->set_body(@$d);
	}
	return ();
}

sub News::Article::signer {
	$_[0]->{'signer'}
}

1;
