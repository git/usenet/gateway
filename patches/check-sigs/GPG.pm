package GPG;
require 5.003;

# Copyright 1998 by Marco d'Itri <md@linux.it>
# You can use this software under the terms of the GPL. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.

# A lot of code has been borrowed from PGP::Sign by Russ Allbery.
# Some idea comes from PGP::Pipe.

# The interface should probably be rewritten in a cleaner way.

# NOTE: this module is looking for a new maintainer. Please adopt and
# start maintaining it, I can't do better than this.

use FileHandle;
use IPC::Open3;

use strict;
use vars qw($VERSION);

$VERSION = '0.2';

sub new {
	my $class=shift;
	my $gpgpath=shift || undef;
	my $gpgbin=shift || "/usr/local/bin/gpg";

	my $self={
		GPGPATH => $gpgpath, GPGBIN => $gpgbin,
		_data => [], _errors => []
	};

	bless $self, $class;

	$self;
}

sub Exec {
	my ($self, $opts, %args)=@_;

	# Ignore SIGPIPE, since we're going to be talking to PGP.
	local $SIG{PIPE}='IGNORE';

	my @command=($self->{GPGBIN}, '--batch', '--status-fd=2');
	my $passfh;
	if (defined $args{Pass}) {
		$passfh=new FileHandle;
		my $writefh=new FileHandle;
		pipe ($passfh, $writefh);
		print $writefh $args{Pass};
		close $writefh;
		push (@command, '--passphrase-fd='.$passfh->fileno);
	}

	push (@command, '--homedir', $self->{GPGPATH})
		if (defined $self->{GPGPATH});
	push (@command, '--homedir', $args{Homedir})
		if (defined $args{Homedir});
	push (@command, '--keyring', $args{Keyring})
		if (defined $args{Keyring});
	push (@command, '--armor')
		unless (defined $args{Armor} and $args{Armor}==0);
	push (@command, '--textmode')
		unless (defined $args{Textmode} and $args{Textmode}==0);

	if (defined $self->{_debug}) {
		foreach (@command, @$opts) { print STDOUT "$_ " }; print STDOUT "\n";
	}

	my $in=new FileHandle;
	my $data=new FileHandle;
	my $errors=new FileHandle;
	my $pid=eval { IPC::Open3::open3($in, $data, $errors, @command, @$opts) };
	if ($@) {
		$self->{_errors}=[$@, "Execution of $command[0] failed.\n"];
		return undef;
	}

	# Send our data to the child
	write_data($in, $args{Data}) if (defined $args{Data});

	close $in;
	$self->{_data}=[<$data>];
	$self->{_errors}=[<$errors>];
	close $data;
	close $errors;
	close $passfh if (defined $passfh);
	waitpid ($pid, 0);

	if ($? > 1) {
		push (@{$self->{_errors}},
			"$command[0] returned exit status ", $? >> 8 , "\n");
		return undef;
	}
	return '';
}

sub Sign {
	my ($self, %args)=@_;

	foreach (qw(Pass Data)) {
		die "$_ not defined" unless (defined $args{$_});
	}

	my @opts;
	if (defined $args{Detach}) {
		@opts=('--detach-sign');
	} else {
		@opts=('--clearsign');
	}
	if (defined $args{Key}) {
		push (@opts, '--local-user', $args{Key});
	}

	my @r=$self->Exec(\@opts, %args);
	return undef if not defined @r;
	return @{shift->{_data}};
}

sub Verify {
	my ($self, %args)=@_;

	$args{Data} || die 'Data not defined';

	my @opts;
	my @r=$self->Exec(\@opts, %args);
	return undef if not defined @r;

	my %reply;	# would the caller want to use this hash?
	foreach (@{$self->{_errors}}) {
		next if (not /^\[GNUPG:\]\s+(\S+)\s?(.*)/);
		$reply{$1}=$2;
	}
	#use Data::Dumper;print STDERR Data::Dumper->Dump([\%reply],["reply"]);exit;
	#use Data::Dumper;print STDERR Data::Dumper->Dump([\$self], ["self"]);exit;
	if (defined $reply{GOODSIG}) {
		my ($id, $name)=split(/ /, $reply{GOODSIG}, 2);
		return wantarray ? (0, $id, $name, $self->{_data}) : $id;
	}
	if (defined $reply{BADSIG}) {
		my ($id, $name)=split(/ /, $reply{BADSIG}, 2);
		return wantarray ? (1, $id, $name, $self->{_data}) : '';
	}
	if (defined $reply{ERRSIG}) {
		return wantarray ? (2, 'ERRSIG', '', $self->{_data}) : '';
	}
	return undef;
}

sub Export {
	my ($self, %args)=@_;

	die 'Key not defined' unless defined $args{Key};

	my @opts;
	if (ref $args{Key} eq "ARRAY") {
		@opts=map(('--export', $_ ), @{$args{Key}});
	} else {
		@opts=('--export', $args{Key});
	}

	my @r=$self->Exec(\@opts, %args);
	return undef if not defined @r;
	return @{shift->{_data}};
}

sub Data {
	return @{shift->{_data}};
}

sub Errors {
	return join('', @{shift->{_errors}});
}

sub Debug {
	my ($self, $arg)=@_;
	return $self->{_debug}=(1 || $arg);
}

# Russ Allbery wrote this useful routine.
sub write_data {
	my $fh = shift;
	my $source;
	for $source (@_) {
		if (ref $source eq 'ARRAY' or ref $source eq 'HASH') {
			for (@$source) { print $fh $_ }
		} elsif (ref $source eq 'GLOB' or ref \$source eq 'GLOB') {
			local $_;
			while (<$source>) { print $fh $_ }
		} elsif (ref $source eq 'SCALAR') {
			print $fh $$source;
		} elsif (ref $source eq 'CODE') {
			local $_;
			while (defined ($_ = &$source ())) { print $fh $_ }
		} elsif (ref $source eq 'REF') {
			print $fh $source;
		} elsif (ref $source) {
			if ($] > 5.003) {
				if (UNIVERSAL::isa ($source, 'IO::Handle')) {
					local $_;
					while (<$source>) { print $fh $_ }
				} else {
					print $fh $source;
				}
			} else {
				if (ref $source eq 'FileHandle') {
					local $_;
					while (<$source>) { print $fh $_ }
				} else {
					print $fh $source;
				}
			}
		} else {
			print $fh $source;
		}
	}
}

1;
