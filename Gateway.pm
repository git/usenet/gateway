# News::Gateway -- Mail and news gatewaying toolkit.  -*- perl -*-
#
# Copyright 1997-1999 by Russ Allbery <eagle@eyrie.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.
#
# "Obviously unlike you people, I don't have time to edit the newsgroups
# line for every single article I post."
#                        -- markl@cs.yale.edu, in the midst of a huge
#                           flamewar about COBOL crossposted to too many
#                           newsgroups including alt.folklore.computers.

############################################################################
# Modules, declarations, and site configuration
############################################################################

package News::Gateway;
require 5.003;

use News::Article ();

use strict;
use vars qw($AUTOLOAD $FAILCODE $VERSION);

# The current News::Gateway package version number, which is not the same as
# the CVS revision of this file.
$VERSION = 0.42;

# Exit code that should be used to indicate permanent failure.  This should
# be 100 for qmail and probably be 64 for sendmail (EX_USAGE), although you
# may want to pick some other sendmail status (there are so many to choose
# from....).
$FAILCODE = 64;


############################################################################
# Gateway module loading
############################################################################

# We can't quite use AutoLoader, although we're going to do something very
# similar, because the name of the file to be imported isn't going to be the
# same as the called sub.  We're being very strange, tricky, and subtle.
# The name of the module to load will be the sub we tried to call, with any
# trailing underscore and letters after an underscore stripped off and a .al
# appended.  We'll look in the standard autoload directory for it.  Most of
# this code is derived from AutoLoader.
sub AUTOLOAD {
    my ($package, $module) = ($AUTOLOAD =~ /(.*)::([^:]+)$/);
    $package =~ s%::%/%;
    $module =~ s/_[^_]+$//;

    # If our package has been loaded, which it should have been, we can
    # derive the name of the directory our modules are in from it.  Fall
    # back to a full @INC search.
    my $name = $INC{"$package.pm"};
    if ($name) {
        $name =~ s%^(.*)$package\.pm$%$1auto/$package/$module.al%;
        undef $name unless (-r $name);
    }
    unless ($name) { $name = "auto/$package/$module.al" }

    # Now comes the fun.  We try to load the module.  If the routine trying
    # to be autoloaded is DESTROY or import, we generate a null one on the
    # fly to avoid annoying error messages at shutdown or use time.
    #
    # We have to handle a few things specially here.  First, if the sub that
    # we're trying and failing to autoload is mail_error() (which may be
    # called by error()), then we have a major problem and need to avoid
    # calling more methods.  Second, if the first argument to the sub isn't
    # a reference (meaning that they're not calling a method), we can't then
    # call our error() method since we don't have an object.  (This should
    # really be fixed by making error() robust in that case....)
    my $save = $@;
    eval { require $name };
    if ($@) {
        $@ =~ s/ at .*\n//;
        if (substr ($AUTOLOAD, -9) eq '::DESTROY'
            || substr ($AUTOLOAD, -8) eq '::import') {
            no strict qw(refs);
            *$AUTOLOAD = sub {};
            use strict qw(refs);
        } elsif (not ref $_[0]) {
            $! = $FAILCODE;
            die "Autoload of $AUTOLOAD failed: $@\n"
                . "Attempted to autoload non-method, aborting\n";
        } else {
            $_[0]->error ("Autoload of $AUTOLOAD failed: $@");
        }
    }
    $@ = $save;
    goto &$AUTOLOAD;
}


############################################################################
# Constructor and accessors
############################################################################

# Create a new gateway object.  Take one optional argument specifying the
# envelope sender to be used for mail we send out.  If this isn't specified,
# no particular envelope will be assumed.
sub new {
    my $class = shift;
    $class = ref $class || $class;
    my $self = { Envelope => shift };
    bless ($self, $class);
}

# Return the underlying article handled by the gateway object.  If given an
# argument, this is taken to be a new News::Article reference that should
# become the underlying article.  This isn't checked; it's assumed the
# caller knows what they're doing.
sub article {
    my $self = shift;
    $$self{Article} = shift if @_;
    $$self{Article};
}

# Return or set the envelope sender used when sending mail, similarly.
sub envelope {
    my $self = shift;
    $$self{Envelope} = shift if @_;
    $$self{Envelope};
}

# Return or set the moderator an article is assigned to, similarly.
sub moderator {
    my $self = shift;
    $$self{Moderator} = shift if @_;
    $$self{Moderator};
}


############################################################################
# Message handling
############################################################################

# Read in the article from a source, returning undef if this fails (either
# because no data is available or because the size limits are exceeded) and
# the number of bytes read if it succeeds.  This is mostly just a wrapper
# around the News::Article constructor.
sub read {
    my $self = shift;
    $$self{Article} = News::Article->new (@_);
}

# Apply a list of modules in order, which means running module_mesg for each
# one.  This method should return undef for success or a textual error
# message on failure.  If any module fails, apply aborts at that point and,
# in a scalar context, returns the failed module name, a colon, a space, and
# the message.  In a list context, it returns the failure message and then a
# list of all of the unapplied modules (the first of which will be the
# module that failed).  On success, undef (or the empty list) is returned.
sub apply {
    my ($self, @modules) = @_;
    while (@modules) {
        my $module = $modules[0];
        my $method = $module . '_mesg';
        my $error = $self->$method ();
        if ($error) {
            return wantarray ? ($error, @modules) : "$module: $error";
        }
        shift @modules;
    }
}

# Clear all of the stored configuration data for a given list of modules.
sub clear {
    my ($self, @modules) = @_;
    for (@modules) { delete $$self{$_} }
}


############################################################################
# Error handling
############################################################################

# Handle a fatal script error.  This shouldn't be used for article
# rejections, only for syntax errors, fatal system problems, and the like.
sub error {
    my ($self, $error) = @_;
    if ($$self{Error}) {
        &{$$self{Error}} ($error);
    } else {
        $! = $FAILCODE;
        die $error . "\n";
    }
}

# Set (or return) an error handler, called by News::Gateway::error().  Takes
# a code ref which will be called in the case of an error with the error as
# the only argument.  Returns the current handler.
sub error_handler {
    my $self = shift;
    $$self{Error} = shift if @_;
    $$self{Error};
}

1;
